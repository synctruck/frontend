/* Definición Léxica */
%lex

%options case-insensitive

%%

/* Special literals */
"'"([^"'"\\]|\\.)*"'"		                      		{return 'CHAR_STRING';}
"\""([^"\""\\]|\\.)*"\""		                    	{return 'STRING';}
"??"												                  		{return 'HELPER';}
[0-9]+th                                          {return 'INDEX';}
SYNC[0-9]+						                            {return 'SYNC_ID';}
[a-zA-Z]([^"@" \t\r\n])*"@"[^ \t\r\n]+            {return 'EMAIL';}
[01]?[0-9]\/[0123]?[0-9]\/[0-9]{2}([0-9][0-9])?   {return 'DATE'}
[a-zA-Z]{3}\/[0123]?[0-9]\/[0-9]{2}([0-9][0-9])?   {return 'DATE_MONTH_FIRST'}
[0123]?[0-9]\/[a-zA-Z]{3}\/[0-9]{2}([0-9][0-9])?   {return 'DATE_MONTH_SECOND'}

/* Filter keywords */
"is"            						                {return 'IS';}
("null"|"unavailable"|"empty")\b	   				{return 'NULL';}
("includes"|"contains")\b	         					return 'CONTAINS';
"from"			            			         			return 'FROM';
"to"								                      	return 'TO';
"not"					              			          return 'NOT';
"and"         		                         	return 'AND';
"or"					                      		  	return 'OR';
"in"							                        	return 'IN';
"starts"						                        return 'STARTS';
"with"                                      return 'WITH';
"ends"				                        			return 'ENDS';
"desc"                                      return 'DESC';
"asc"                                       return 'ASC';
("row"|"rows")\b                            return 'ROW';
"valid"                                     return 'VALID';
"duplicate"                                 return 'DUPLICATE';
"invalid"                                   return 'INVALID';
"incomplete"                                return 'INCOMPLETE';

/* Off limits keywords */
"search"					          		return 'SEARCH';
"display"					          		return 'DISPLAY';
"filter"							        	return 'FILTER';
"order"								          return 'ORDER';
"by"								            return 'BY';
"grant"		             					return 'GRANT';
"revoke"						          	return 'REVOKE';
"create"			      						return 'CREATE';
"logout"                        return 'LOGOUT';
"permission"                    return 'PERMISSION';
"import"                        return 'IMPORT';
"preview"                       return 'PREVIEW';
"enable"                        return 'ENABLE';
"manual"                        return 'MANUAL';
"mode"                          return 'MODE';
"save"                          return 'SAVE';
"commit"                        return 'COMMIT';
"purge"                         return 'PURGE';
"delete"                        return 'DELETE';
"update"                        return 'UPDATE';
"edit"                          return 'EDIT';
"clone"                         return 'CLONE';
"prepare"                       return 'PREPARE';
"times"                         return "TIMES";
"current"                       return "CURRENT";
"set"                           return "SET";
"for"                           return "FOR";
"submit"                        return "SUBMIT";
"cleanse"                       return "CLEANSE";
"fuse"                          return "FUSE";

/* Soft keywords */
"module"                        return 'MODULE_LITERAL';
"full"                           return 'FULL';
"access"                        return 'ACCESS';
"explicit"                      return 'EXPLICIT';
"help"                          return 'HELP';

/* Available modules: */

("drivers"|"driver")\b           return 'DRIVER';
("users"|"user")\b               return 'USER';
("vans"|"van")\b                 return 'VAN';
("station"|"stations")\b         return 'STATION';

/* Symbols & operators */
":"                               return 'COLON';
"("		              							return 'LEFT_PAREN';
")"							              		return 'RIGHT_PAREN';
"="						              			return 'IGUAL';
"!="				            					return 'DISTINTO';
">="							              	return 'MAYORIGUAL';
"<="					               			return 'MENORIGUAL';
">"					              				return 'MAYORQ';
"<"								              	return 'MENORQ';
","                               return 'COMMA';

/* Other */

([a-zA-Z]|_)+[0-9_A-Za-z]*        {return 'ID';}
(-)?[0-9]+("."[0-9]+)?		   			{return 'NUMBER';}
[ \r\t]+                                {/*WHITESPACE IGNORE*/}
\n                                      {/*NEW LINE. IGNORE*/}
<<EOF>>                 			           return 'EOF';
.										                    {throw HELP_COMMANDS['value'];}

/lex

/* Asociación de operadores y precedencia */
%left OR
%left AND
%left COMPARACION DISTINTO MAYORQ MENORQ MAYORIGUAL MENORIGUAL
%left UNOT

%start s_0

%% /* Definición de la gramática */

s_0 : command EOF {__root = $1;};

command: 	searchStmt {$$ = $1;}
		|filterStmt {$$ = new RQuery('filter',{filter:$1}); }
		|orderStmt {$$ = new RQuery('order',{"order_by":$1});}
		|createStmt {$$ = $1;}
		|displayStmt {$$ = $1;}
    |bulkStmt {$$ = $1;}
		|grantStmt {$$ = $1;}
    |revokeStmt {$$ = $1;}
    |previewStmt {$$ = $1;}
    |manualStmt {$$ = $1;}
    |editStmt {$$ = $1;}
    |LOGOUT {$$ = new RQuery('logout');}
    |HELP HELPER {throw HELP_COMMANDS['base'];}
    |HELP {throw HELP_COMMANDS['base'];}
    |HELPER {throw HELP_COMMANDS['base'];}
    ;

previewStmt : PREVIEW MODULE {$$ = new RQuery('preview',{module:$2}); }
            | PREVIEW MODULE filterStmt {$$ = new RQuery('preview',{module:$2,filter:$3}); }
            | PREVIEW MODULE orderStmt {$$ = new RQuery('preview',{module:$2,order_by:$3}); }
            | PREVIEW MODULE filterStmt orderStmt {$$ = new RQuery('preview',{module:$2,filter:$3,order_by:$4}); }
            | PREVIEW {$$ = new RQuery('preview',{}); }
            | PREVIEW filterStmt {$$ = new RQuery('preview',{filter : $2}); }
            | PREVIEW orderStmt {$$ = new RQuery('preview',{order_by : $2}); }
            | PREVIEW filterStmt orderStmt {$$ = new RQuery('preview',{filter: $2, order_by :$3}); }
            | COMMIT {$$ = new RQuery('commit'); }
            | SAVE {$$ = new RQuery('save');}
            | PURGE {$$ = new RQuery('purge');}
            | CLEANSE {$$ = new RQuery('cleanse');}
            | FUSE {$$ = new RQuery('fuse');}
            | UPDATE setClause FOR indexL ROW {$$ = new RQuery('update',{updates:$2,times:1,targets:$4}); }
            | UPDATE setClause FOR SyncL {$$ = new RQuery('update',{updates:$2,times:1,targets:$4}); }
            | UPDATE setClause FOR CURRENT SET {$$ = new RQuery('update',{updates:$2,times:1,targets:[]}); }
            | UPDATE HELPER {throw HELP_COMMANDS['update'];}
            | DELETE CURRENT SET { $$ = new RQuery('delete',{targets:[],updates:[],times:1}); }
            | DELETE indexL ROW {$$ = new RQuery('delete',{targets:$2,updates:[],times:1}); }
            | DELETE SyncL {$$ = new RQuery('delete',{targets:$2,updates:[],times:1}); }
            | DELETE HELPER {throw HELP_COMMANDS['delete'];}
            | CLONE indexL ROW NUMBER TIMES { $$ = new RQuery('clone',{targets:$2,times:$4,updates:[]});}
            | PREPARE NUMBER ROW  {$$ = new RQuery('prepare',{targets:[],times:$2,updates:[]});}
            | PREVIEW MODULE PREPARE NUMBER ROW {$$ = new RQuery('prepare_preview',{module:$2,targets:[],times:$4,updates:[]});}
            | PREVIEW HELPER {throw HELP_COMMANDS['preview'];}
            ;

editStmt : EDIT MODULE {$$ = new RQuery('edit',{module:$2});}
          | EDIT MODULE filterStmt {$$ = new RQuery('edit',{module:$2,filter:$3});}
          | EDIT MODULE filterStmt orderStmt {$$ = new RQuery('edit',{module:$2,filter:$3,order_by:$4});}}
          | EDIT MODULE orderStmt {$$ = new RQuery('edit',{module:2,order_by:$3});}
          | EDIT {$$ = new RQuery('edit',{});}
          | EDIT filterStmt {$$ = new RQuery('edit',{filter:$2});}
          | EDIT filterStmt orderStmt {$$ = new RQuery('edit',{filter:$2,order_by:$3});}
          | EDIT orderStmt {$$ = new RQuery('edit',{order_by : $2});}
          | EDIT SyncL {$$ = new RQuery('prepare_edit',{targets:$2});}
          | EDIT CURRENT SET  {$$ = new RQuery('prepare_edit',{targets:[]});}
          | EDIT HELPER {throw HELP_COMMANDS['edit'];}
          ;

manualStmt : ENABLE MANUAL MODE {$$ = new RQuery('enable_manual'); }
            | SUBMIT              {$$ = new RQuery('submit');}
            | MANUAL MODE HELPER {throw HELP_COMMANDS['manual']}
            | ENABLE MANUAL MODE HELPER {throw HELP_COMMANDS['manual'];}
            ;

indexL : indexL COMMA INDEX {$1.push(/[0-9]+/.exec($3)[0]); $$ = $1;}
            | INDEX {$$ = [/[0-9]+/.exec($1)[0]];}
            ;

setClause : setClause COMMA FilterKey IGUAL setAtomic {$1.push({key:$3.getValue(),value:$5}); $$ = $1;}
            | FilterKey IGUAL setAtomic {$$ = [{key:$1.getValue(),value:$3}];}
            ;

setAtomic : NUMBER {$$ = $1; }
            		| CHAR_STRING {$$ = $1.substring(1,$1.length-1); }
            		| STRING {$$ = $1.substring(1,$1.length-1);}
            		| DATE  {$$ = $1 }
            		| EMAIL {$$ = $1 }
            		| SYNC_ID {$$ = /[0-9]+/.exec($1)[0];}
            		;

displayStmt : DISPLAY {$$ = new RQuery('resume_display',null); }
            | DISPLAY MODULE SYNC_ID {$$ = new RQuery('display',{module:$2,id:new Value('id',$3).getValue()}); }
            | DISPLAY SYNC_ID {$$ = new RQuery('display',{module:null,id:new Value('id',$2).getValue()}); }
            | DISPLAY HELPER {throw HELP_COMMANDS['display'];}
            ;

bulkStmt :  IMPORT MODULE {$$ = new RQuery('import',{module:$3}); }
          | IMPORT {$$ = new RQuery('import',{module:null});}
          | IMPORT HELPER {throw HELP_COMMANDS['bulk'];}
          ;

createStmt :  CREATE MODULE {$$ = new RQuery('create',{module:$2}); }
            | CREATE {$$ = new RQuery('create',{module:null});}
            | CREATE HELPER {throw HELP_COMMANDS['create'];}
            ;

grantStmt :   GRANT PERMISSION permissionL TO SyncL {$$ = new RQuery('grant',{users : $5, targets: $3, station:false});}
            | GRANT FULL ACCESS TO SyncL {$$ = new RQuery('grant',{users : $5, targets: [], station:false});}
            | GRANT MODULE_LITERAL ModuleList TO SyncL {$$ = new RQuery('grant',{users : $5, targets: $3, station:false});}
            | GRANT STATION IdList TO SyncL {$$ = new RQuery('grant',{users : $5, targets: $3, station:true});}
            | GRANT FULL STATION ACCESS TO SyncL {$$ = new RQuery('grant',{users : $5, targets: [], station:true});}
            | GRANT HELPER {throw HELP_COMMANDS['grant'];}
            ;

revokeStmt :   REVOKE PERMISSION permissionL TO SyncL {$$ = new RQuery('revoke',{users : $5, targets: $3, station:false});}
            | REVOKE FULL ACCESS TO SyncL {$$ = new RQuery('revoke',{users : $5, targets: [], station:false});}
            | REVOKE MODULE_LITERAL ModuleList TO SyncL {$$ = new RQuery('revoke',{users : $5, targets: $3, station:false});}
            | REVOKE STATION IdList TO SyncL {$$ = new RQuery('revoke',{users : $5, targets: $3, station:true});}
            | REVOKE FULL STATION ACCESS TO SyncL {$$ = new RQuery('revoke',{users : $6, targets: [], station:true });}
            | REVOKE HELPER {throw HELP_COMMANDS['revoke'];}
            ;

filterStmt : FILTER Exp {$$ = $2}
            | FILTER HELPER {throw HELP_COMMANDS['filter'];}
            ;

orderStmt : ORDER BY KeyL {$$ = {fields:$3,direction:"ASC"};}
            | ORDER BY KeyL DESC {$$ = {fields:$3,direction:"DESC"};}
            | ORDER BY KeyL ASC {$$ = {fields:$3,direction:"ASC"};}
            | ORDER BY HELPER {throw HELP_COMMANDS['order'];}
            ;


searchStmt : SEARCH Key filterStmt orderStmt
{
                  $$ = new RQuery('search',
                  {
                    module: $2.getModule(),
                    version: $2.getVersion(),
                    filter : $3,
                    order_by : $4
                  });
}
            | SEARCH Key filterStmt
{
                  $$ = new RQuery('search',
                  {
                    module: $2.getModule(),
                    version: $2.getVersion(),
                    filter : $3
                  });
}
            | SEARCH Key orderStmt
{
                  $$ = new RQuery('search',
                  {
                    module: $2.getModule(),
                    version: $2.getVersion(),
                    order_by : $3
                  });
}
            | SEARCH Key
{
                  $$ = new RQuery('search',
                  {
                    module: $2.getModule(),
                    version: $2.getVersion()
                  });
}
            | SEARCH Key FROM date_literal TO date_literal orderStmt
{
                  $$ = new RQuery('search',
                  {
                    module: $2.getModule(),
                    version: $2.getVersion(),
                    filter : new Filter("contains",null,[$4.getValue(),$6.getValue()]),
                    order_by : $7
                  });
}
            | SEARCH Key FROM date_literal orderStmt
{
                  $$ = new RQuery('search',
                  {
                    module: $2.getModule(),
                    version: $2.getVersion(),
                    filter : new Filter("contains",null,$4.getValue()),
                    order_by : $5
                  });
}
            | SEARCH Key FROM date_literal TO date_literal
                {
                  $$ = new RQuery('search',
                  {
                    module: $2.getModule(),
                    version: $2.getVersion(),
                    filter : new Filter("contains",null,[$4.getValue(),$6.getValue()])
                  });
                }
            | SEARCH Key FROM date_literal
{
              $$ = new RQuery('search',
              {
                module: $2.getModule(),
                version: $2.getVersion(),
                filter : new Filter("from",null,$4.getValue())
              });
}
            | SEARCH Key CONTAINS text_literal orderStmt {
            $$ = new RQuery('search',
{
              module: $2.getModule(),
              version: $2.getVersion(),
              filter : new Filter("contains",null,$4.getValue()),
              order_by : $5
});}
            | SEARCH Key CONTAINS text_literal {$$ = new RQuery('search',
            {
              module: $2.getModule(),
              version: $2.getVersion(),
              filter : new Filter("contains",null,$4.getValue())
            });}
            | SEARCH HELPER {throw HELP_COMMANDS['search'];}
            ;

SyncL : SyncL COMMA id_literal {$1.push($3.getValue()); $$ = $1;}
        | id_literal {$$ = [$1.getValue()];}
        ;

IdList : IdList COMMA ID {$1.push($3); $$ = $1;}
        | ID {$$ = [$1];}
        ;

ModuleList : ModuleList COMMA MODULE {$1.push($3); $$ = $1;}
            | MODULE {$$ = [$1]; }
            ;

permissionL : permissionL COMMA KeyUnit COLON KeyUnit {$1.push(new Permission($3,$5)); $$ = $1;}
            | KeyUnit COLON KeyUnit {$$ = [new Permission($1,$3)];}
            ;

MODULE : DRIVER {$$ = "DRIVER";}
        | VAN {$$ = "VAN";}
        | USER {$$ = "USER";}
        | STATION {$$ = "STATION"}
        ;

Key : Key KeyUnit {$1.push_key($2); $$ = $1;}
    | KeyUnit {$$ = new Key($1); }
    ;

KeyL : KeyL COMMA FilterKey {$1.push($3.getValue()); $$ = $1;}
      | FilterKey {$$ = [$1.getValue()];}
      ;

KeyUnit : ID {$$ = $1;}
          | FULL {$$ = $1}
          | ACCESS {$$ = $1}
          | EXPLICIT {$$ = $1}
          | HELP {$$ = $1}
          | DRIVER {$$ = $1}
          | USER {$$ = $1}
          | VAN {$$ = $1}
          | STATION {$$ = $1}
          ;

FilterKey : MODULE_LITERAL {$$ = new Key($1);}
          | Key {$$ = $1;}
          ;

Exp : InferredKeys {$$ = $1;}
    | FilterKey FROM date_literal TO date_literal {$$ = new Filter("from_to",$1,[$3.getValue(),$5.getValue()])}
    | FilterKey FROM date_literal {$$ = new Filter("from",$1,$3.getValue())}
    | FilterKey STARTS WITH text_literal {$$ = new Filter("starts_with",$1,$4.getValue())}
    | FilterKey ENDS WITH text_literal {$$ = new Filter("ends_with",$1,$4.getValue())}
    | FilterKey CONTAINS text_literal {$$ = new Filter("contains",$1,$3.getValue())}
		| FilterKey MAYORQ atomic {$$ = new Filter(">",$1,$3.getValue())}
		| FilterKey MENORQ atomic {$$ = new Filter("<",$1,$3.getValue())}
		| FilterKey MAYORIGUAL atomic {$$ = new Filter(">=",$1,$3.getValue())}
		| FilterKey MENORIGUAL atomic {$$ = new Filter("<=",$1,$3.getValue())}
		| FilterKey IGUAL atomic {$$ = new Filter("=",$1,$3.getValue())}
		| FilterKey DISTINTO atomic {$$ = new Filter("!=",$1,$3.getValue())}
		| Exp AND Exp {$$ = new Filter("and",null,null,$1,$3);}
		| Exp OR Exp {$$ = new Filter("or",null,null,$1,$3);}
    | NOT Exp %prec UNOT {$$ = new Filter("not",null,null,$2,$2); }
		| LEFT_PAREN Exp RIGHT_PAREN {$$ = $2;}
		| FilterKey IN LEFT_PAREN atomList RIGHT_PAREN {$$ = new Filter("in",$1,$4); }
		| FilterKey NOT IN LEFT_PAREN atomList RIGHT_PAREN {$$ = new Filter("not_in",$1,$5); }
		| FilterKey IS NULL {$$ = new Filter("is_null",$1,null); }
		| FilterKey IS NOT NULL {$$ = new Filter("is_not_null",$1,null); }
		| ROW IS VALID {$$ = new Filter("=",new Key('color'),'green');}
		| ROW IS DUPLICATE {$$ = new Filter("=",new Key('color'),'yellow');}
		| ROW IS INVALID {$$ = new Filter("=",new Key('color'),'red');}
		| ROW IS INCOMPLETE {$$ = new Filter("=",new Key('color'),'orange');}
		;

InferredKeys: FROM date_literal { $$ = new Filter("from",null,$2.getValue()) }
              | FROM date_literal TO date_literal { $$ = new Filter("from_to",null,[$2.getValue(),$4.getValue()]) }
              | CONTAINS text_literal { $$ = new Filter("contains",null,$2.getValue()); }
              | STARTS WITH text_literal {$$ = new Filter("starts_with",null,$3.getValue());}
              | ENDS WITH text_literal {$$ = new Filter("ends_with",null,$3.getValue());}
              ;

atomList : atomList COMMA atomic { $1.push($3.getValue()); $$ = $1;}
       | atomic {$$ = [$1.getValue()];}
       ;

id_literal : SYNC_ID {$$ = new Value('id',$1);}
            ;

number_literal : NUMBER {$$ = new Value('number',$1);}
                | HELPER {throw HELP_COMMANDS['number'];}
                ;

date_literal : DATE  {$$ = new Value('date',$1); }
          		| DATE_MONTH_FIRST  {$$ = new Value('date_month_first',$1); }
		          | DATE_MONTH_SECOND  {$$ = new Value('date_month_second',$1); }
              | HELPER {throw HELP_COMMANDS['date'];}
              ;

text_literal : EMAIL {$$ = new Value('email',$1);}
    | CHAR_STRING {$$ = new Value('char_string',$1); }
    | STRING {$$ = new Value('string',$1);}
    | HELPER {throw HELP_COMMANDS['text'];}
    ;

atomic : NUMBER {$$ = new Value('number',$1); }
		| CHAR_STRING {$$ = new Value('char_string',$1); }
		| STRING {$$ = new Value('string',$1);}
		| SYNC_ID {$$ = new Value('id',$1);}
		| DATE  {$$ = new Value('date',$1); }
		| DATE_MONTH_FIRST  {$$ = new Value('date_month_first',$1); }
		| DATE_MONTH_SECOND  {$$ = new Value('date_month_second',$1); }
		| EMAIL {$$ = new Value('email',$1);}
		| HELPER {throw HELP_COMMANDS['value'];}
		;
