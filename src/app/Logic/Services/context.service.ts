import { Injectable } from '@angular/core';
import {Log} from '../Utility/Log';
import {RDisplay} from '../Utility/RDisplay';
import {Option} from '../Grammar/Option';
import {Help, HELP_COMMANDS} from '../Utility/Help';
import {Spreadsheet} from '../Utility/Bulk';
import {PAGE_SIZE} from '../Utility/Tools';

@Injectable({
  providedIn: 'root'
})
export class ContextService {
  public aux : any;
  public commandEnabled : boolean;
  public action : string = 'modules';
  public primary : Array<string> = [
    'loading'
  ];
  public secondary : Array<string> = [
    'loading'
  ];
  public spreadsheets: Spreadsheet[];
  public backupModule: string ;
  public readonly MAX_MANUAL_SIZE = 150;
  public readonly PREVIEW_VERSION = 776;
  public readonly EDITION_VERSION = 777;
  public pageSize: number;
  public log_book: Log[];
  public module: string;
  public version: number;
  public view: string;
  public display: RDisplay;
  public fields: string[][];
  public table: any[] ;
  public tableSettings: any[];
  public selected_station: Option;
  public selected_module: Option ;
  public available_stations: Option[];
  public available_modules: Option[];
  public rHelp: Help;
  public loading: boolean;
  public token: number;
  public isAuthenticated: boolean;
  public index: number;
  public recordCount: number;
  public manual_records: any;
  public manual_headers: any[];
  public manual_data: any[];
  public available_tables: string[];
  public utility_array: any[];
  filesBuffer: object;
  constructor() {
    this.aux = undefined;
    this.utility_array = [];
    this.commandEnabled = false;
    this.available_tables = [];
    this.spreadsheets = [];
    this.pageSize = PAGE_SIZE;
    this.table = [{loading: '....'}];
    this.tableSettings = [{primaryKey: 'loading', header: 'Loading', format: 'default'}];
    this.selected_station =  new Option('loading', 'loading');
    this.selected_module = new Option('loading', 'loading');
    this.log_book = [];
    this.module = 'Loading';
    this.view = 'Loading';
    this.available_modules = [];
    this.available_stations = [];
    this.rHelp = HELP_COMMANDS['base'];
    this.loading = false;
    this.token = 0;
    this.isAuthenticated = false;
    this.index = 0;
    this.recordCount = 0;
    this.fields = [];
    this.display = null;
    this.manual_records = {};
    this.manual_headers = [];
    this.manual_data = [];
    this.backupModule = this.module;
    this.filesBuffer = null;
  }
  update(v: any){
    this.recordCount = v.max;
    this.table = v.table;
    this.tableSettings = v.settings;
    this.module = v.module;
    this.version = v.version;
  }
  compile_manual_query() {
    const res = [];
    for (const indice in this.manual_records) {
     // const r = {index : indice};
      let r = 'update ';
      const v = this.manual_records[indice];
      for (const jk in v) {
        let jm = v[jk].value;
        if(!jm) jm = 'null';
        jm = jm.toString().trim();
        if(jm=='')jm = 'null';
        jm = jm.replace("'","''");
        jm = `"${jm}"`; //Alright, jm is now a highly
        //curated value suitable to be sent to the server
        //and trusted.
        r = `${r} ${jk} = ${jm},`;
      }
      r = r.substring(0,r.length-1); //We remove the extra ,
      r += 'for '+indice;
      res.push(r);
    }
    return res.join(';');
  }
  public backup(){
    this.backupModule = this.module;
  }
  public restore(){
    this.module = this.backupModule;
  }
}
