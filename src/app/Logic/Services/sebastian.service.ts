import {RModalComponent} from '../../rmodal/rmodal.component';
import {MatDialog} from '@angular/material/dialog';

const DEBUG_MODE = false;
const lambdaUrl = 'https://33g3fhgzt4.execute-api.us-east-2.amazonaws.com/Production';
const baseUrl =  DEBUG_MODE?'http://192.168.0.21:9777':lambdaUrl;
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import {DomSanitizer} from '@angular/platform-browser';
import {Help, HELP_COMMANDS} from '../Utility/Help';
import {Router} from '@angular/router';
import {RDisplay} from '../Utility/RDisplay';
import {ContextService} from './context.service';
import {Option} from '../Grammar/Option'

@Injectable({
  providedIn: 'root'
})
export class SebastianService {
  private readonly ARGS = 'jkjmvjhrmsgjin';
  private readonly R_CLASS = 'R_CLASS';
  private readonly R_FUNCTION = 'R_FUNCTION';
  private readonly R_CLASSES = {
    'Option': Option,
    'R_DISPLAY' : RDisplay
  };
  private readonly R_FUNCTIONS = {
    'change_applicant_status': (id,options)=>{
      const dialogRef = this.dialog.open(RModalComponent, {
        data: {
          command : 'change_applicant_status',
          id: id,
          options : options
        }
      });
      dialogRef.afterClosed().subscribe(result => result?this.command(result):undefined);
    },
    'getFieldGrid' : ()=> this.Context.display.getFieldGrid(),
    'selectFirstStation' : () => this.Context.selected_station = this.Context.available_stations[0]
  } ;
  /* ARGS is an special key which indicates that a data object
  * returned by the server must NOT be used directly.
  * Instead, it is meant to be used as the arguments for a
  * constructor of a class indicated in the R_CLASS key.
  * i.e:
  * data = { R_CLASS = 'Overview', ARGS = [...] }
  * This Object indicates that because it has ARGS as a key it
  * must NOT be assigned directly to Context. Instead, we
  * must build a new R_CLASS instance (in this case Overview)
  * And use ARGS as the arguments for the constructor of such class.
  * */
  constructor(private http: HttpClient, private toastr: ToastrService,
              private sanitizer: DomSanitizer, private Ciel: Router,private Context : ContextService,private dialog : MatDialog) {}
  login(loginForm:any) {
    return this.request('post', `${baseUrl}/login`,loginForm);
  }
  logout(){
    this.request('get',`${baseUrl}/logout/${this.Context.token}`).then(()=>{
      this.Context.isAuthenticated = false;
      this.Context.token = 0;
    });
  }
  public command(statement : string) : void {
    if(/\?\?/.test(statement))return this.show_help(statement);
    const req = {
      token : Number(this.Context.token),
      stmt : statement
    };
    this.Context.loading = true;
    this.request('post', `${baseUrl}/interpret`,req).then((v)=>{
      this.Context.loading = false;
      const { message, data, view } = v;
      this.inform(message);
      for(const key in data){
        const val = data[key];
        if(typeof val === 'object'){
          if(this.ARGS in val){
            const r_args = val[this.ARGS];
            if(this.R_CLASS in val){
              const r_class = this.R_CLASSES[val[this.R_CLASS]];
              if(!r_class) {
                console.error('The server requests a new instance of an ' +
                  'Undefined class. Class: '+val[this.R_CLASS],this.R_CLASSES);
                return;
              }
              this.Context[key] = new r_class( ...r_args );
              continue;
            } else if (this.R_FUNCTION in val){
              const r_function = this.R_FUNCTIONS[val[this.R_FUNCTION]];
              if(!r_function) return console.error('Server requests undefined function: '+val[this.R_FUNCTION],this.R_FUNCTIONS);
              this.Context[key] = r_function( ...r_args );
              continue;
            } else console.error('Special key ARGS has been detected. However, value is not a Class or a Function.',val);
          }
        }
        this.Context[key] = val;
      }
      if(view){
        this.Ciel.navigateByUrl(`/${view}`).then(()=>{
          this.Context.view = view;
        })
      }
    }).catch((err)=>{
      console.log(err);
      this.Context.loading = false;
      this.error(err.error['message']);
    })
  }
  load_defaults(){
    this.command('[list-modules]');
    this.command('[list-stations]');
    this.resetSearch();
  }

  //region Search

  fetch_table(){
    this.command(`[fetch] ${this.Context.pageSize} ${this.Context.index}`);
  }

  resetSearch() {
    this.Context.recordCount = 0;
    this.Context.index = 0;
    this.fetch_table();
  }
  prevPage() {
    if(this.Context.index<=0)return;
    this.Context.index = this.Context.index - this.Context.pageSize;
    this.fetch_table();
  }
  nextPage() {
    if(this.Context.index + this.Context.pageSize > this.Context.recordCount)return;
    this.Context.index = this.Context.index + this.Context.pageSize;
    this.fetch_table();
  }
  //endregion
  save_record() {
    const query = this.Context.display.compile();
    this.command(query); //We update the normal properties of the module.
    if(this.Context.display.record.primary && this.Context.display.record.secondary){
      this.Context.primary = this.Context.display.record.primary;
      this.Context.secondary = this.Context.display.record.secondary;
      const q = '[primary] '+
        this.Context.primary.map(p=>`"${p}"`).join(',')+
        ' [secondary] '+this.Context.secondary.map(p=>`"${p}"`).join(',');
      this.command(q);
    }
    console.log(query);
  }

  //endregion
  show_help(s : string){
    //Customer is asking for help!
    if(/filter\s*\?\?/i.test(s))this.getHelp(HELP_COMMANDS['filter']);
    else if(/^value\s*\?\?/i.test(s))this.getHelp(HELP_COMMANDS['value']);
    else if(/filter\s+[^?]+\?\?/i.test(s))this.getHelp(HELP_COMMANDS['value']);
    else if (/(^\?\?)|(help)|(help\s*\?\?)/i.test(s))this.getHelp(HELP_COMMANDS['base']);
    else if (/^search\s*\?\?/i.test(s)) this.getHelp(HELP_COMMANDS['search']);
    else if (/order\s+by\s*\?\?/i.test(s)) this.getHelp(HELP_COMMANDS['order']);
    else if (/^preview[^?]*\?\?/i.test(s)) this.getHelp(HELP_COMMANDS['preview']);
    else if (/^create\s+\?\?/i.test(s)) this.getHelp(HELP_COMMANDS['create']);
    else if (/^update\s+\?\?/i.test(s)) this.getHelp(HELP_COMMANDS['update']);
    else if (/^delete\s+\?\?/i.test(s)) this.getHelp(HELP_COMMANDS['delete']);
    else if (/^purge\s+\?\?/i.test(s)) this.getHelp(HELP_COMMANDS['purge']);
    else if (/^cleanse\s+\?\?/i.test(s)) this.getHelp(HELP_COMMANDS['cleanse']);
    else if (/^prepare\s+\?\?/i.test(s)) this.getHelp(HELP_COMMANDS['prepare']);
    else if (/^display\s+\?\?/i.test(s)) this.getHelp(HELP_COMMANDS['display']);
    else if (/^commit\s+\?\?/i.test(s)) this.getHelp(HELP_COMMANDS['commit']);
    else if (/^fuse\s+\?\?/i.test(s)) this.getHelp(HELP_COMMANDS['fuse']);
    else if (/^save\s+\?\?/i.test(s)) this.getHelp(HELP_COMMANDS['save']);
    else if (/^integrity\s+\?\?/i.test(s)) this.getHelp(HELP_COMMANDS['integrity']);
    else if (/^color\s+\?\?/i.test(s)) this.getHelp(HELP_COMMANDS['integrity']);
    else if (/^color\s+codes\s+\?\?/i.test(s)) this.getHelp(HELP_COMMANDS['integrity']);
    else if (/^edit\s+\?\?/i.test(s)) this.getHelp(HELP_COMMANDS['edit']);
    else if (/^manual\s+\?\?/i.test(s)) this.getHelp(HELP_COMMANDS['manual']);
    else if (/^manual\s+mode\s+\?\?/i.test(s)) this.getHelp(HELP_COMMANDS['manual']);
    else if (/^enable\s+manual\s+mode\s+\?\?/i.test(s)) this.getHelp(HELP_COMMANDS['manual']);
    else if (/^revoke\s+\?\?/i.test(s)) this.getHelp(HELP_COMMANDS['revoke']);
    else if (/^date\s+\?\?/i.test(s)) this.getHelp(HELP_COMMANDS['date']);
    else if (/^date\s+format\s+\?\?/i.test(s)) this.getHelp(HELP_COMMANDS['date']);
    else this.getHelp(HELP_COMMANDS['base']);
    return ;
  }
  submit_manual_mode(){
    if(this.Context.view !== 'manual') return this.error('Submit command applies only while in Manual Mode.');
    const q = this.Context.compile_manual_query();
    this.command(q);
  }
  getHelp(e: Help) {
    this.Context.rHelp = e;
    this.Ciel.navigateByUrl('/help');
  }
  resume_display(){
    if(!this.Context.display)return this.error(`You haven't displayed any record yet!`);
    this.Ciel.navigateByUrl('/display').then(()=>{this.Context.view = 'display';});
  }
  //region Utils
  private request(method: string, url: string, data?: any,response_type:"json"|"text"|"arraybuffer" = 'json') {
    const result = this.http.request(method, url, {
      body: data,
      responseType: response_type,
      observe: 'body'
    });
    return new Promise<any>((resolve, reject) => {
      result.subscribe(resolve as any, reject as any);
    });
  }
  success(message:string){
    if(!message)return;
    this.toastr.success(message);
  }
  inform(message:any){
    if(!message)return;
    /*This method should check a mesage and display appropiate alerts.*/
    let errors = [/error/i,/denied/i,/fatal/i,/cannot/i];
    let isError:boolean = false;
    for (const error1 of errors) {
      if(error1.test(message))isError = true;
    }
    if(isError)this.toastr.error(message);
    else this.toastr.info(message);
  }
  error(message:any){
    if(!message)return;
    this.toastr.error(message);
  }
  //endregion
  //region Import
  importSheets(){
    return this.error("Import features are currently under maintenance. Please, try again later")
  }

  registerNewSheet(id: string) {
    return this.error("Import features are currently under maintenance. Please, try again later")
  }

  selectSheet(spreadsheetIndex: number) {
    return this.error("Import features are currently under maintenance. Please, try again later")
  }
  processSheet(index: number, sheet: string, action: string) {
    return this.error("Import features are currently under maintenance. Please, try again later")
  }

  //endregion

  takeAction(action) : boolean {
	    //Only update Context's action when needed.
    this.Context.commandEnabled = action == 'command';
    if(['table','workspace','edit','more','modules'].includes(action))this.Context.action = action;
    switch (action){
      case 'profile': this.command('[profile]'); break;
      case 'logout' : this.logout(); break;
      case 'import' : this.importSheets(); break;
      case 'guide' : this.command('??'); break;
      case 'workspace' : this.command('preview'); break;
      case 'table':
      case 'command':
      case 'more':
      case 'modules':
          return false;
      default : return true;
    }
    return false;
  }
  performAction(action){
    switch (action){
      case 'create_many':
      case 'clone' :
      case 'delete':
        return true;
      case 'create_1':{
        this.command('create');
        return false;
      }
      case 'manual_mode':{
        this.command('enable manual mode '+
          this.Context.pageSize+' '+this.Context.index);
        return false;
      }
      case 'commit':
      case 'save':
        if(this.Context.view === 'manual') {
          this.submit_manual_mode();
          break;
        }
      default : {
        this.command(action);
        return false;
      }
    }
  }

  async registerFile(fbody : object) {
    this.Context.loading = true;
    let r = await this.request('post',`${lambdaUrl}/files`,fbody);
    this.Context.loading = false;
    return r.url;
  }
  getFiles(id:number){
     return this.request('get',`${lambdaUrl}/files?module=${this.Context.module}&id=${id}`);
  }

  display_files(v: any) {
    this.Context.filesBuffer = v;
   this.Ciel.navigateByUrl('/files').then(v=>{this.Context.view = 'files'});
  }
}
