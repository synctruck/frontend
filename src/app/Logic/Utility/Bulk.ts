export class Sheet {
  title : string;
  rowCount : string;
  columnCount : string;
  overview : Overview;
}
export class Overview{
  settings : any[];
  table : any[];
}
export class Spreadsheet{
  id : string;
  sheets : Sheet[];
  title : string;
  timezone : string;
  url : string;
}
