export const char_size = 5*2.3;
export class ColumnMap {
  primaryKey: string;
  private _header?: string;
  private readonly bag : any;
  alternativeKeys?: string[];
  constructor ( settings:any ) {
    this.primaryKey = settings.primaryKey;
    this.header = settings.header;
    this.alternativeKeys = settings.alternativeKeys;
    this.bag = settings;
  }
  set header(setting: string) {
    this._header = setting ?
      setting :
      this.primaryKey.slice(0, 1).toUpperCase() +
      this.primaryKey.replace(/_/g, ' ' ).slice(1)
  }
  get header() {
    return this._header;
  }
  access = function ( object: any ) :string {
    if (object[this.primaryKey] || !this.alternativeKeys) {
      return this.primaryKey;
    }
    for (let key of this.alternativeKeys) {
      if (object[key]) {
        return key;
      }
    }
    return this.primaryKey;
  }
  public get_background(record : any){
    let bc;
    if('background_color' in this.bag)bc = record[this.bag['background_color']];
    else bc = undefined;
    if(!bc)return null;
    switch (bc) {
      case 'green':return '#c4ffc4';
      case 'red':return '#ff9898';
      case 'yellow':return '#ffff86';
      case 'orange':return '#f9df82';
      case 'blue':return '#7ebeff';
      case 'purple':return '#c79cff';
      case 'brown': return '#c19970';
      default:return 'initial';
    }
  }
  public get_link(record:any):string{
    if('link' in this.bag){
      if(this.bag.link === 'default'){
        if('id_display' in record)return `display ${record['id_display']}`;
        if('id' in record)return `display SYNC${/[0-9]+/.exec(record['id'])[0]}`;
        return null;
      }else {
        return record[this.bag.link];
      }
    }
    if('cell_link' in this.bag)return record[this.bag['cell_link']];
    if('col_link' in this.bag)return this.bag['col_link'];
    return null;
  }
  public get_color(record:any):string{
    if('cell_color' in this.bag)return record[this.bag['cell_color']];
    if('col_color' in this.bag)return this.bag['col_color'];
    return 'black';
  }

  get_type() {
    if('cell_type' in this.bag)return this.bag['cell_type'];
    return 'label';
  }

  get_options() {
    return this.bag['options'];
  }
}
