//Documentation :
export class Description{
  name:string;
  body:string;
  constructor(name:string,description:string) {
    this.name = name;
    this.body = description;
  }
}
export class Example{
  clickable:boolean;
  content:string;
  link:string;
  bold : boolean;
  constructor(content:string,clickable:boolean = false,bold:boolean = false) {
    this.content = content;
    this.clickable = clickable;
    this.link = this.content.trim();
    this.bold = bold;
  }
}
export class Help{
  description:Description;
  suggestions:Array<Help>;
  examples:Array<Example>;
  constructor(n:string,d:string) {
    this.description = new Description(n,d);
    this.suggestions = [];
    this.examples=[];
  }
  setExamples(ex:Array<Example>):Help{
    //this.description.body+= ' Examples include:';
    this.examples = ex;
    return this;
  }
  setSuggestions(h:Array<Help>):Help{
    //this.description.body += ' It has several variants, each of them is explained below:';
    this.suggestions = h;
    return this;
  }
}


//region VALUE types documentation
export const HELP_COMMANDS = {
  'id': new Help(`ID expected`,`IDs are special values used by Synctruck to identify each record
    of a table. Therefore IDs are generated automatically by the System and are always unique for each record within a table.
     IDs are the safest way to refer to any record individually. And are used by many commands like DISPLAY, GRANT, UPDATE and many more.
    They can also be used to filter tables. All the valid Synctruck IDs have the following format: SYNC[number]
     Where SYNC is the SynctruckID prefix followed by a number. Whenever you need to perform a search on an ID column or need to specify
    an ID for a command, make
    sure to use the ID directly and not to quote it, as they are not text! `).setExamples([
    new Example('FILTER id in (SYNC1,SYNC2,SYNC3,SYNC44)'),
    new Example('DISPLAY driver SYNC55'),
    new Example('FILTER id != SYNC44 or id = SYNC885')
  ]),
  'date':
    new Help(`DATE expected`,`A date is defined as follows: dd/mm/yyyy.
    Where dd refers to day, you can use single digit days directly or prefix with 0
    if you want to. mm is the month of the year, if it is a single digit month you can use it directly
     or prefix it with 0. yyyy is the year, you can use a 2 digit year if you want to, in which
    case it is assumed you are referring to a 2000s year.`).setExamples([
      new Example('01/05/2019'),
      new Example('8/10/17'),
      new Example('4/3/1997')
    ]),
  'text':
    new Help(`TEXT expected`,`The most common type of fields a record holds is text (like name, or email),
    there are many ways in which you can express text each of them is defined as follows:`).setSuggestions([
      new Help(`Simple Quoted Text`,`The most common way of expressing text is by typing it
        and then wrapping it in simple quotes ('). Examples are:`).setExamples(
        [ new Example("'renato'"),
          new Example("'Has not logged in'"),
          new Example("'A'")]
      ),
      new Help(`Double Quoted Text`,`Exactly the same as Simple Quoted Text, but you an can use
        double quotes (") To wrap the text rather than simple quotes if you feel like it. It works exactly
        the same, is just a matter of preference. Examples are:`).setExamples(
        [ new Example('"renato"'),
          new Example('"Has not logged in"'),
          new Example('"A"')]
      ),
      new Help(`Emails`,`Emails are they only type of text that does not need to be wrapped
      in quotes. Since they are treated as text and have no special behavior wrapping emails with or without
       quotes have exactly the same meaning and treated equally. The idea behind this type of value
      is to save you time by not having to wrap the emails in quotes if used directly. Examples include: `)
        .setExamples([
          new Example('renatojosuefloresperez@gmail.com'),
          new Example('aaronburr@economy.princeton.university.edu.us'),
          new Example('marcoflores@global.support.com')
        ])
    ]),
  'number':
    new Help(`NUMBER expected`,`Numbers are expressed directly. There is no need to
     wrap them with any kind of special symbol (in fact, if you wrap a number with quotes it will be treated as text and not as a number)
    The system supports all types of rational numbers.`).setExamples([
      new Example('120'),
      new Example('-78954'),
      new Example('1245.4478'),
      new Example('-0.457')
    ]),
  'key': new Help(`KEY expected`,`KEY refers to either the name of a table
    or the name of a column. For names that have more than one word like all drivers for example
     you must not wrap them with any symbol. In fact, if you do it will be treated as text rather than a name
     and you will get a Syntax error. To refer to names with more than one word, simple type them normally and
    the system will recognize it.`).setExamples([
    new Example('all drivers'),
    new Example('date added'),
    new Example('full name')
  ])
};
HELP_COMMANDS['value'] = new Help(`VALUE expected`,`Value refers to any of the four types
  of values that can be used with the system. Which are: DATE,TEXT,NUMBER & ID.`).setSuggestions([
  HELP_COMMANDS['date'],HELP_COMMANDS['number'],HELP_COMMANDS['text'],HELP_COMMANDS['id']
]);
//endregion
//region MODULE documentation
HELP_COMMANDS['module'] = new Help(`MODULE expected`,`Most important commands accept a Module parameter which indicates the type
  of record you want to perform operations into. Popular modules are displayable. This means
  the module will appear in your left side navigation bar. Other not so popular modules wont appear there, but the modules
  still exist and you can perform your usual operations on them. To check which modules you have access to, type the command: Search Help.
  A module must have at least one searchable table, otherwise is not considered a Module.
  Currently, the System supports the following modules: `).setSuggestions([
  new Help(`DRIVER`,`Editable: Yes. Displayable: Yes.`),
  new Help(`USER`,`Editable: Yes. Displayable: Yes.`),
  new Help(`VAN`,`Editable: Yes. Displayable: Yes.`),
  new Help(`STATION`,`Editable: Yes. Displayable: No.`),
  new Help(`HELP`,`Editable: No. Displayable: Yes.`),
  new Help(`HISTORY`,`Editable: No. Displayable: No.`)
]);
//endregion
//region FILTER documentation
HELP_COMMANDS['filter'] = new Help(`FILTER Clause Explanation`,`All tables are "filterable". You can use filters
   as stand alone commands, in which case, the command will filter the table you are currently viewing. This means that you can
  filter a table once and then perform another filter on the resulting table. You can repeat this process (Chained Filtering),
   as many times as you need until you find your desired data. You can also use filters as part of a search command.
   Filters are very powerful and flexible. To use them, simply type the FILTER keyword on the COMMAND BAR followed by a filter expression.
   Filter expressions available are the following: `).setExamples([
  new Example('Search drivers FILTER status includes "active" and email ends with "@gmail.com"',true),
  new Example('Search drivers FILTER substatus is not empty',true),
  new Example('FILTER from 6/4/2020',true)
])
  .setSuggestions([
    new Help(`Contains`,`Most filter expressions require you to specify the -field- or -column name- from the table upon
      which you are about to apply the filter. However, the 'CONTAINS' keyword is an special filter which allows you to
      apply the filter to the whole table and all it's columns/fields. This operation searches all occurrences within any column
      which has the specified text as part of it. It is a non case-sensitive search, which means it does not distinguish between Lowercases and Uppercases
      to perform the search. If you need a case-sensitive search, use the equal (=) operation instead.`).setExamples([
      new Example('FILTER contains "jua"'),
      new Example('FILTER full name contains "collins"'),
      new Example('FILTER driver license contains "24578"')
    ]),
    new Help(`Includes`,`This expression is a basically a synonym of the 'contains' keyword expression. You can use either
      depending on your preference on syntax. They both have exactly the same effect.`).setExamples([
      new Example('FILTER includes "jua"'),
      new Example('FILTER full name includes "collins"'),
      new Example('FILTER driver license includes "24578"')
    ]),
    new Help(`Starts with`,` This expression is similar to 'CONTAINS', however, it searches only those columns
      that start with the searched text. Just like the 'contains' expression, you can omit the column name that you
      are searching, if you do, all text columns are searched. It performs a non case-sensitive search too.`)
      .setExamples([
        new Example('FILTER full name starts with "a"'),
        new Example('FILTER starts with "ann"'),
        new Example('FILTER phone number starts with "5471"')
      ]),
    new Help(`Ends with`,`Similar to starts with. The difference is it searches all rows
      where the searched column ends with the specified search text.`).setExamples([
      new Example('FILTER email ends with "gmail.com"'),
      new Example('FILTER ends with "Adams"'),
      new Example('FILTER phone number ends with "0101"')
    ]),
    new Help(`From to`,`This filter is meant to be used with dates. It searches
       all rows where the searched column is within the searched date range. The searched column name
      can be omitted, in which case the default date field of the table is searched. If a table has no date
      columns an error will be prompted.`).setExamples([
      new Example('FILTER from 20/4/2019 to 1/2/2020'),
      new Example('FILTER date added from 05/09/18 to 7/12/2019')
    ]),
    new Help(`From`,`This filter is similar to the from to expression, however this one
      assumes the closer limit in the range is the current-day date. It will search all rows
      from the specified date to today.`).setExamples([
      new Example('FILTER from 7/6/2016'),
      new Example('FILTER from 15/07/2020'),
      new Example('FILTER date added from 9/4/12')
    ]),
    new Help(`Comparisons`,`'Comparisons' is not an actual filter clause, but it refers
       to many filtering clauses where the usual comparison operators are used.
       The supported comparison operators are: (=,>,<,<=,>=,!=).
      Where != is the not equal operator. All of these operators can be used in any of the three types of
      available values. When used with text, the comparisons are case-sensitive. The -field/column name-
       where the search needs to be performed must always be specified.`).setExamples([
      new Example('FILTER full name = "Marco Flores"'),
      new Example('FILTER full name != "Aaron Burr"'),
      new Example('FILTER age >= 35'),
      new Example('FILTER date_added <= 20/8/2019')
    ]),
    new Help(`And`,`The 'AND' expression is a very powerful expression. It allows you to join
      two or more expressions and returns all rows that match the conditions of the specified expression. If
      at least one of the joined expressions is not met by a row, the row won't be shown. The 'AND' operator has a
      higher priority over the 'OR' operator. The 'NOT' operator has the highest priority. <br>
      Whenever you use these kind of operators it is recommended for you to wrap the affected expressions in parenthesis
      if you are combining many of them, this will help you keep track of the expression. This also work with
      numerical, probability and statistical expressions`).setExamples([
      new Example('FILTER full name starts with "Diana" and age >= 25'),
      new Example('FILTER full name ends with "Kent" and age < 45 and date added <= 20/11/2019'),
      new Example('FILTER id (SYNC24,SYNC445,SYNC879) and from 5/5/2020')
    ]),
    new Help(`or`,`The 'OR' expression is similar to and, however it returns a valid value if at least one
      of the joined expressions is met, unlike 'AND', which returns valid data if all the joined conditions are true.
      It can be combined with as many expressions are needed and can also be combined with other filters/operators.
      Remember, 'AND' takes higher priority over 'OR', while NOT has the highest priority.`).setExamples([
      new Example('FILTER id = SYNC245 or name starts with "m"'),
      new Example('FILTER (from 23/8/2019 or email ends with "@gmail.com") and (full name ends with "jackson")'),
      new Example('FILTER id = SYNC447 and email includes "global" or id = (SYNC2,SYNC1,SYNC77)'),
      new Example('FILTER id = SYNC1 or id = SYNC2 or id = SYNC3 or id = SYNC4 or id = SYNC5')
    ]),
    new Help(`not`,`The 'NOT' expression is similar to 'AND' & 'OR' in the sense that it is applied to multiple
       filter expression. It negates the result of the searching condition. It can be used with any expression.
      It is recommended for you to wrap the argument of the 'NOT' operator with parenthesis to avoid confusion when reading.`)
      .setExamples([
        new Example('FILTER not (id = SYNC245) or not (name starts with "h")'),
        new Example('FILTER not (id = SYNC1 or id = SYNC2 or id = SYNC3 or id = SYNC4 or id = SYNC5)'),
        new Example('FILTER from 4/5/17 and not id in (SYNC55,SYNC33,SYNC77)')
      ]),
    new Help(`in`,`The 'IN' operator is intended to be used for selecting specific values from a larger range or table.
       For instance, you want the system to show records for some specific dates, however, they don't have any correlation with each other.
       In this situation you could either, create a long command using the 'AND,OR & NOT' operators, or you could use the IN operator instead.
       The IN operator accepts any group of values separated by commas and wrapped in parenthesis. All values from an 'IN' search must always be of the same type.`).setExamples([
      new Example('FILTER id in (SYNC4,SYNC5,SYNC6,SYNC7,SYNC8)'),
      new Example('FILTER date added in (10/2/20,14/7/19,15/03/17,04/10/2020)'),
      new Example('FILTER not (full name in ("Renato Flores","Catherine Jones", "Anne Rivas"))')
    ]),
    new Help(`is`,`Most records usually have one or more optional fields. Whenever an optional field has not been
      filled yet but the record is shown, the value will be show as empty, under a gray color with the caption unavailable.
      If you try a text inquiry like: name = unavailable won't grant the desired results. This means any 'unavailable' value
      can't be used for any comparison! Therefore, if a numeric or date field is optional and happens to be empty, you can by no means
      try to perform a text search for unavailable! <br>
      For these kind of situations an special operator has been introduced: The 'IS' operator. You can use it to search for every row where the value is empty/unavailable/null.
      Keep in mind that the keywords: empty, unavailable & null. Are all synonyms and work exactly for the same purpose, you
      can use the one you like the most. If you are trying to find rows where the value of any column is not empty, you can either
      use the NOT operator as usual, or if you prefer a more natural-like syntax you can type: 'column IS not empty'. As you can see,
      the NOT keyword was developed for a more humanlike, grammar-friendly syntax. Both are valid options <br>
      Also notice how empty,null,unavailable are keywords with special meaning and therefore they must not be quoted to make comparisons.
      If for whatever reason you'd like to use them as text make sure to quote them.`).setExamples([
      new Example('FILTER age is not empty'),
      new Example('FILTER age is unavailable and full name starts with "r"'),
      new Example('FILTER alt phone is empty or email is empty or birth date is empty')
    ])
  ])
//endregion
//region Order By Documentation.
HELP_COMMANDS['order']= new Help(`ORDER BY Clause Explanation`,`An 'ORDER BY' will allow you to sort any result/table in current view.
 It takes 2 parameters:  A list of column/field names (you can also use only one column name which is the most common case), separated by comma.
  followed by an orientation keyword: 'DESC' or 'ASC'. The orientation keyword tells the system the direction you want for the sorting. It can be ascendant (ASC)
  or descendant (DESC). The orientation is optional, so if no direction is specified 'DESC' is taken as default. <br>
  Specifying a list of column names instead of just one is useful when there are ties. For this situations they system will pick the first field for sorting,
  while if there are ties among the sorted records, the second column name is used to sort those records.
  If there are ties again and you provided a third column name it is used for the next sorting and so on.
  Whenever there are no ties but you provided more than one column/field name, the extra column names will be ignored.`).setExamples([
  new Example('Order by date added desc'),
  new Example('Order by full name asc'),
  new Example('search help Order by module,version desc'),
  new Example('Order by date added,full name,age desc')
])
//endregion
//region SEARCH documentation;
HELP_COMMANDS['search'] = new Help(`SEARCH Clause Explanation`,`The 'SEARCH' clause is one of the most important & most powerful commands available.
  It is used to show the results of any table. With this command, tables can be shown completely or can include a 'FILTER' clause to show a more specific table.
  Besides the filtering option,  you can also sort the results by adding an optional 'ORDER BY' clause. There are several variants for this command, each of them are described below.`)
  .setSuggestions([HELP_COMMANDS['key']]).setExamples([
    new Example('search help',true),
    new Example('search help filter ??',true),
    new Example('search help filter ?? order by ??',true),
    new Example('search help order by ??',true),
    new Example('search help from ??',true),
    new Example('search help from ?? to ??',true),
    new Example('search help includes ??',true),
    new Example('search help from ?? order by ??',true),
    new Example('search help from ?? to ?? order by ??',true),
    new Example('search help includes ?? order by ??',true)
  ]);
//endregion
//region REVOKE documentation
HELP_COMMANDS['revoke'] = new Help(`REVOKE Clause Explanation`,`This command allows to control
  over the resources a user can access as well as what actions a user can perform. You must have
  access to the REVOKE action in order to use this command.`).setSuggestions([
  new Help(`REVOKE PERMISSION [permission-list] TO [id-list]`,`This is the most explicit
      version of the command. It allows you to revoke access to specified permission list for the specified
      list of users. The list of users is a list of SynctruckIDs separated by comma. A permission is expressed like:
      MODULE:ACTION where MODULE is the name of the module you are revoking access and ACTION is the name of the action
      you are revoking access. There a permission list would look like: MODULE1:ACTION1,MODULE2:ACTION2,...`),
  new Help(`REVOKE MODULE [module-name] TO [id-list]`,`
    This command allows you to revoke all access on the specified module to the specified list of users.`)
    .setSuggestions(HELP_COMMANDS['module']),
  new Help(`REVOKE STATION [station-name] TO [id-list]`,` this command allows you to revoke access
    on a the specified station to the specified list of users.`),
  new Help(`REVOKE ALL ACCESS TO [id]`,`This command allows you to revoke complete access to the platform
    for the specified user. Once this command has been issued the affected user will no longer be able to login in the system.
    You cannot use a list of Users as the target as you need to be very careful when using it.`),
  new Help(`REVOKE ALL STATION ACCESS TO [id]`,`Revokes access for all stations to the affected user.
    A user with access to no stations is considered as inactive and will not be able to login while the user
    remains in this state.`)
]);
//endregion
//region GRANT documentation
HELP_COMMANDS['grant'] = new Help(`GRANT Clause`,`This command does the opposite to revoke.
  It grants access rather than revoking it, the syntax and versions available
  are exactly the same as REVOKE but with the GRANT keyword instead of the REVOKE keyword.
  You can use it to grant access to any specific permission (module & action) a whole module,
  all stations, an specific station or grant access to all resources & actions within the system. Be
  careful when using it!`);
//endregion
//region BULK documentation
HELP_COMMANDS['bulk'] = new Help(`BULK LOAD command expected.`,`
  The Bulk load command is very simple, it allows you to open the Bulk Load wizard for importing data from
  Google-Sheets into the system. It has a vary simple syntax described as follows: bulk load [module-name]`).setSuggestions([
  HELP_COMMANDS['module']
]).setExamples([new Example('bulk load drivers'),new Example('bulk load vans')]);
//endregion
//region CREATE documentation
HELP_COMMANDS['create'] = new Help(`CREATE command expected`,`This command has a very simple syntax
  and it allows you to open the creation wizard for creating records manually rather than importing them. It
  has 2 variations:`).setSuggestions([
  new Help(`create [module-name]`,`This command tells the System you want to quickly create a new record of the
      specified module. This means optional fields are not prompted by the wizard at the moment of creation and you can fill them later.
      As you can see, this is the default.`),
  new Help(`create [module-name] explicit`,`The explicit keyword tells the System you want to create a new record
    of the specified module and that you want to fill all optional fields for each new record at the moment of creation.`)
])
//endregion
//region DISPLAY documentation
HELP_COMMANDS['update'] = new Help('Update',`While Manual Mode already to provides you with a quick &
efficient way to update your data, there might be times when it is tedious to update the same field to the same
value for several records. To help you perform this tasks, you can use this command. It expects a set
of field = value pairs separated by comma and then you can state the targets by Index or ID or you can also
use the CURRENT SET version. If you use Current Set, make sure you've filtered your current set as apropiate.`).setExamples([
  new Example(`Update phone number = '54142824' for current set`),
  new Example(`update status = 'Inactive',substatus = 'Terminated FOR Current Set`),
  new Example(`Update birth date = 3/9/1998 for Sync1,Sync2,Sync8,Sync300`),
  new Example(`Update status = 'Active' FOR 55th,557th,63th rows`)
])
HELP_COMMANDS['delete'] = new Help('Delete',`While Purge, Fuse & Cleanse enable to quickly clean your workspace,
there might be times when you'd like to delete certain sets of rows for whatever reason. To do so, you can use this command.
It has 2 variants: You can specify the index of the rows to delete separated by commas (IDs if in edition mode)
or you can filter your workspace & when you are ready, use the CURRENT SET version to delete your currently seen set.
Keep in mind if you use this command with Current Set & you haven't performed any filter, all your workspace for that
module & version will be lost. Be careful when using it.`).setExamples([
  new Example('delete 13th, 54th, 77th, 304th rows'),
  new Example('delete Sync1, Sync300,Sync2450,Sync23'),
  new Example('Delete Current Set')
])
HELP_COMMANDS['purge'] = new Help('Purge',`Similar to Cleanse, it takes no parameters & the objective
is to clean your workspace, however this command is more severe than Cleanse as it will delete all
not valid rows from your workspace leaving only green rows (valid records).`)
HELP_COMMANDS['cleanse'] = new Help('Cleanse',`Similar to Fuse, this command takes no arguments
and is available in both Edition & Creation mode. It cleanses your workspace by removing all invalid,
corrupt and incomplete records (red,brown & orange respectively). Leaving only valid, unchanged & duplicate
records available for you to proceed with your tasks.`);

HELP_COMMANDS['display'] = new Help(`Display Command Clause`,`Just like the SEARCH command presents
  you tables of data, this command shows you detailed information regarding an specific record.
  Most records usually hold too many fields to be presented in a single table and therefore it makes it very
  difficult to visualize the data which is the main objective.
  In order to display full information & details regarding an specific record you can click on it within a table
  or you can use the 'DISPLAY' command. `).setSuggestions([
  new Help(`DISPLAY [module-name] [id]`,`This is the most explicit version of the command and
      allows you to indicate the module the ID belongs to.`),
  new Help(`DISPLAY [id]`,`This is the most common & recommended version of the command. It allows you to quickly show the information
    of any record identified by [id]. The module the ID belongs to is inferred from your currently seen table.`),
  new Help(`DISPLAY`,`If you use this command directly, you will navigate to your last display.
  If you leave your display page without saving, those changes will not take place in the database. If you
  click on the same record you were displaying again after leaving, all not saved changes will be lost.
  In order to prevent this from happening, you can use the DISPLAY command to go back to your last display session
  and all not saved changes will still be there so you can resume your tasks. If this is the first command you perform
  (without displaying an actual record for the first time in your session) It will fail.`)
])
//endregion
//region PREVIEW DOCUMENTATION
HELP_COMMANDS['commit'] = new Help('Commit',`The most important command for both Preview Versions.
When in Creation Preview mode, all valid records are inserted into the master table & removed from your workspace.
All non-committed records are left un-toached on your workspace. When in Edition mode, the changes you've performed
will take effect on the Master table. Only valid records are subject to update.`);
HELP_COMMANDS['save'] = new Help('Save',`Both preview modes: Creation & Edition delete your workspaces when
you logout. This is meant to keep resource usage optimal for the System. However, you can tell the System not
to do so and instead to reload your workspace on your next login by using this command. This command takes no parameters.
 Keep in mind  that saved changes using this command are only saved to preview and are NOT saved to the Master tables.`);
HELP_COMMANDS['fuse'] = new Help('Fuse',`When you have at your disposal commands like Clone or Prepare
you might eventually reach an stage where you feel overwhelmed by so many yellow rows you might find it
confusing or even frustrating to know what you are doing. To help you quickly recover from such scenario
use the Fuse command. It takes no parameters & deletes all duplicate records in your workspace & leaves
only one of each.`).setExamples([new Example('fuse',true)]);
HELP_COMMANDS['clone'] = new Help('Clone',`It is very common for anyone to copy-paste rows when
you identify certain pattern in the data you are working with. To help you achieve this goal in a
more optimal & elegant way use the Clone command. This command expects 2 arguments: The target rows
to be cloned and how many times each should be cloned. It is often used in combo with the prepare mode.
This command is only available while in creation preview. It does not apply in edition preview mode.`).setExamples([
  new Example('clone 17th,1th,2th rows 5 times'),
  new Example('clone 3th row 25 times')
])
HELP_COMMANDS['integrity'] = new Help('Data Integrity',`Synctruck's System is smart enough to
be able to discern between corrupt records & valid records. This feature is specially powerful to prevent you
from accidentally importing or creating corrupt data & therefore producing unexpected behaviours at long scale.
Whenever you enter preview mode whether it is for Creation or Edition it will always display the Integrity level of the
record. In order to help you quickly tell apart one level of the other, it implements a color based code to
identify the level of integrity of the record. Each of these is explained in detail below.
You can filter records as usual or you can also use the COLOR & ROW keywords to filter by integrity checks.
If you filter by row you can use the name of the integrity level of the records you'd like to target.
If you use color the name of the color can be used instead.`).setExamples([
  new Example(`Filter color = 'red' or color = 'brown`,true),
  new Example(`Filter row is invalid or row is duplicate`,true),
  new Example(`Filter color in ('yellow','orange','brown')`,true)
]).setSuggestions([
  new Help('Green',`Green represents the top level of integrity. Green records are valid in all ways relevant
  to the Master table they belong to and are the only ones that are moved to the Master table when you perform a commit. All other
  records are ignored.`),
  new Help('Yellow',`Yellow represents duplicate records. You're likely to be seeing this color the
  most while in preview due to place-holder rows created by the prepare command or cloned rows to aid you generate templates
  of records. Applies only to Creation Preview mode. It detects duplicate records in both, your current workspace
  and the master table. If you are working with a single record in preview mode that appears as yellow, perform a search
  in the actual Master table for it & edit such record instead.`),
  new Help('Purple',`Purple represents Unchanged records. It only applies to Edition Preview mode.
  You are likely to be seeing this color the most while in edition preview mode since records you prepare for edtion
  are imported unchanged.`),
  new Help('Orange',`Orange represents incomplete records. An Orange record might appear if you delete
  or haven't filled a required field within the record.`),
  new Help('Brown',`Brown represents corrupt records. Most tables make references to other tables in one or
  more of their fields. If one or more of these references is violated a brown record will appear.`),
  new Help('red',`Red represents invalid records. This is one of the colors that are likely to appear the most
  as it identifies records which fields are invalid due to format. A field is invalid if: Date format is incorrect when applicable.
  Numbers containing other symbols when applicable. In order to help you quickly fix Brown or Red records, the field font
  that is causing the violation will also be in red or brown so you can quickly identify the error.`)
])
HELP_COMMANDS['prepare'] = new Help('Prepare',`Preview mode allows you to preview
data insertion and delete/purge any corrupt records so you can be sure you're inserting valid data into the System.
However, this doesn't mean the only way to use it is by importing data (which is recommended).
If you want to use 'Preview' to create new data from scratch, you've probably already noticed you won't be able
to do so since Manual Mode only edits current records in preview and most other commands modify data within preview. <br>
This data is supposed to enter the System in 1 of two ways: Importing it or Using this command.
This command allows you to quickly populate N rows of data into preview so you can later start modifying
trough Manual Mode or any of the other commands available. You can populate any number of place-holder rows
you need to work with using this command & you can use it as stand alone while in preview or you can use
it along the Preview command to quickly navigate to preview & have the System prepare the specified amount
of new rows for you to work with.`).setExamples([
  new Example('Prepare 10 rows'), //Creates 10 new place-holder rows
  new Example('Prepare 125 rows'), //Creates 125 new place-holder rows
  new Example('Preview Drivers Prepare 30 rows') //Navigates to preview & creates 30 new place-holder rows
])
HELP_COMMANDS['preview'] = new Help('Preview Mode',`Creation PREVIEW Mode or simply Preview Mode
is a very powerful mode which allows you to quickly import, create & edit data while keeping the integrity of the System's database.
It's main objective is to handle large sets of data, so it comes with many bulk management commands. This mode is very similar
to Edit Mode, both share similar properties but with some slight exceptions inherent to the concept of each.
To get you started, there are 2 ways you can start using preview mode: <br>
The 'PREPARE' & the 'IMPORT' commands. To access to the PREVIEW mode simply type PREVIEW followed by the name of the
module you'd like to set into the preview mode. All data manipulated in this mode is cleaned up & therefore deleted after you logout
from your current session, in order to keep the resource management optimized. If you want to keep changes at your preview mode,
you can do so with the SAVE command which tells the System not to delete your workspace & to load it on your next login.
This command saves your changes on the preview mode only and are not saved to the Official database. <br>
Another option is to use the COMMIT command which tells the System you're ready to save your changes to
the Official database & proceeds to insert all valid records into the Main/Master table of the in-preview module.
One of this mode's biggest strengths is that the System performs integrity checks for each record in your
workspace & makes sure it won't corrupt the database in any way. This is specially useful when importing data
so you don't need to carefully check them before starting to work with them since the System can identify
corrupt records & enable you to quickly dispose of them, edit them
or perform any task you feel suitable before moving the data to the Main/Master table of the module.
Each of these bulk management tools is described in detail in their corresponding sections. As a final side note,
PLEASE KEEP IN MIND you can enter to a 'Manual' Mode whithin Preview mode at any time to quickly edit your data.`).setExamples([
  new Example('preview',true) //Navigation command, takes you to the preview of your currently seen module.
  ,new Example('Preview Drivers',true) //Same as above, but you specify explicitly the module to be accessed.
  ,new Example('Preview Drivers Prepare 10 rows',true) //Hybrid command. It Navigates to Driver's preview & prepares 10 new rows for you to start working with, since your
  //preview is likely to be empty if you haven't imported any data.
]).setSuggestions([
  HELP_COMMANDS['integrity'],
  HELP_COMMANDS['prepare'],
  HELP_COMMANDS['clone'],
  HELP_COMMANDS['fuse'],
  HELP_COMMANDS['cleanse'],
  HELP_COMMANDS['purge'],
  HELP_COMMANDS['save'],
  HELP_COMMANDS['commit'],
  HELP_COMMANDS['delete'],
  HELP_COMMANDS['update']
])
//endregion
//region MANUAL & EDIT documentation
HELP_COMMANDS['edit'] = new Help('Edit Mode',`Edit mode is a very powerful mode which enables you to perform
modifications on several records at once and provides you with several bulk management commands. This mode supports most
of the commands preview does with the exception of Fuse, Clone & Prepare for inherent reasons.
The color-code used in preview mode to describe the integrity checks apply here as well with the exception of
yellow since it is impossible to have duplicate in this mode. Instead, the purple color is introduced to
identified records that have suffered no changes based on their official counterparts. Just like in preview mode,
any changes applied to the records in this mode won't get updated to the official database until you commit them. <br>
Make sure to use the Save command if you need to logout. If your workspace isn't ready to be committed but you want to keep working on it later,
just use the 'SAVE' command. To enter this mode simply type EDIT in the command line, the module to be edited will be inferred & you'll be redirected to your current
edition workspace for that module. It will usually be empty, since you need to specify which records you'd like
to edit. In order to get started with this mode, there are two options available:
You can specify explicitly the IDs of the records you'd like to edit in a list separated by commas or
you can filter your current seen table & specify to edit your current set.`).setExamples([
  new Example('Edit SYNC1,SYNC2,SYNC200,SYNC400'),
  new Example('Edit Current Set'),
  new Example('Edit'),
  new Example('Edit Drivers')
]).setSuggestions([
  HELP_COMMANDS['purge'],
  HELP_COMMANDS['cleanse'],
  HELP_COMMANDS['save'],
  HELP_COMMANDS['delete'],
  HELP_COMMANDS['update'],
  HELP_COMMANDS['commit']
]);
HELP_COMMANDS['manual'] = new Help('Manual Mode',`Manual mode is a very powerful mode which allows you to manually
edit data within a table without the need to use commands or a form for each record. This mode can only be enabled
if you are in preview, create or edit mode. To keep the system optimized & stable, Manual mode
can only support up to 100 records. If your workspace holds more than 100 records, use filters to separate them into
 groups & enable manual mode for each group as needed. If you navigate to another place within the system while in manual mode
 and you didn't save your changes, those will be lost the next time you enable manual mode again! To prevent this from happening
 make sure you use the SUBMIT command when you feel satisfied with the changes you've performed. This command saves those changes
 to your workspace & sends you back to preview. The changes are not saved to the database yet until you commit them as usual.
 As a side note, keep in mind bulk management commands like Clone, Cleanse, Purge, etc. Won't work while in manual mode.
 Make sure you've saved your changes and are back in preview before using those.`);
//endregion
//region BASE documentation
HELP_COMMANDS['base'] = new Help(`Command Help Guide`,`Welcome to the Synctruck System! This is a command-driven system
  which allows you to quickly and efficiently manage your data. It has many advantages over the form-based navigation most people is used to and
   is far more intelligent, straightforward & easy to use! All the commands available are non case-sensitive, which means you do not need to worry about
  the case of the commands, a SEARCH HELP command would produce exactly the same result as 'Search help'. <Br>
  This system includes a HELP command operator (??), which can be used at any time. Type a command followed by this operator whenever you are not sure on what goes next on a certain instruction.
  The system will display a detailed information guide on what is expected by the command.
  Typing the '??' operator directly in the command line displays this guide.`).setExamples([
  new Example('??',true),
  new Example('Search ??',true,true),
  new Example('Filter ??',true,true),
  new Example('Order by ??',true),
  new Example('Create ??',true),
  new Example('Display ??',true),
  new Example('Grant ??',true),
  new Example('Revoke ??',true),
  new Example('Update ??',true),
  new Example('Delete ??',true),
  new Example('Preview ??',true,true),
  new Example('Edit ??',true),
  new Example('Enable Manual Mode ??',true),
  new Example('Logout',true)
])
//endregion
