export class Log{
  performed_at : string;
  responsible : string;
  action : string;
  description : string;
}
