import {ColumnMap} from './ColumnSetting';
export const DATE_FORMAT = /[01]?[0-9]\/[0123]?[0-9]\/[0-9]{2}([0-9][0-9])?/;
export const DATE_MONTH_FIRST_FORMAT = /[a-zA-Z]{3}\/[0123]?[0-9]\/[0-9]{2}([0-9][0-9])?/;
export const DATE_MONTH_SECOND_FORMAT = /[0123]?[0-9]\/[a-zA-Z]{3}\/[0-9]{2}([0-9][0-9])?/;
export const PLACEHOLDER_FORMAT = /^new-/;
export const MONTH_LITERALS = {
  'jan': 1,
  'feb': 2,
  'mar':  3,
  'apr':4,
  'may':5,
  'jun':6,
  'jul':7,
  'aug':8,
  'sep':9,
  'oct':10,
  'nov':11,
  'dec':12
}

export class Field{
  raw:any;
  type: string;
  field_type : string;
  optional : boolean;
  editable : boolean;
  state : string;
  key: string;
  name: string;
  styleConfig : ColumnMap;
  constructor(raw:any,module:string,config:any) {
    if(typeof raw === 'string'){
      this.key = raw;
      this.name = raw;
      this.state = 'display';
      this.optional = false;
      this.editable= false;
      this.field_type = 'label';
      this.type = 'string';
      return;
    }
    if(Array.isArray(raw)){
      if(config){
        if('style' in config){
          let aux = [];
          for (const cell of raw) {
            if(cell.key in config.style){
              let r = {primaryKey:cell.key,header:cell.name};
              let style = config.style[cell.key];
              for (const s in style) {
                r[s] = style[s];
              }
              aux.push(new ColumnMap(r));
            }else aux.push(new ColumnMap({primaryKey:cell.key,header:cell.name}));
          }
          raw = aux;
        }
      }
      this.raw = raw;
      this.key = 'overview';
      this.name = 'Overview';
      this.state = 'display';
      this.optional = false;
      this.editable= false;
      this.field_type = 'label';
      this.type = 'string';
      this.styleConfig = this.raw[0];
      return;
    }
    this.raw = raw;
    this.key = raw.key;
    this.name = raw.name;
    if('type' in raw)this.type = raw.type;
    else this.type = 'string';
    if('field_type' in raw)this.field_type = raw.field_type;
    else this.field_type = 'input';
    if('optional' in raw)this.optional = raw.optional;
    else this.optional = true;
    if('editable' in raw)this.editable = raw.editable;
    else this.editable = true;
    this.state = 'display';
    if(config){
      if('style' in config){
        if(this.key in config.style){
          let r = {primaryKey:this.key,header:this.name};
          let style = config.style[this.key];
          for (const s in style) {
            r[s] = style[s];
          }
          this.styleConfig = new ColumnMap(r);
        }else this.styleConfig = new ColumnMap({primaryKey:this.key,header:this.name});
      }
    }
  }
  getSuggestions(record:any) : string[] {
    if('variable_options' in this.raw){
      let variable_key = this.raw.variable_key;
      let true_key = record[variable_key]; //We get the actual value of the key.
      if(!true_key)return [];
      return this.raw.variable_options[true_key];
    }
    if('options' in this.raw)return this.raw.options;
    return [];
  }
}
export class RDisplay{
  constructor(module:string,title:string,record:any,fields:any[],ren:object,overview?:object[]) {
    //Here the ren parameter makes reference to a config instance.
    this.module = module;
    this.title = title;
    this.record = record;
    this.configuration = ren;
    this.fields = fields.map((f)=>new Field(f,this.module,ren));
    if(overview){
      this.fields.unshift(new Field(overview,this.module,ren))
    }
    this.profile_pic =this.record['profile']; //Can or not be defined.
  }
  profile_pic ?: string;
  module:string;
  title:string;
  record:any;
  fields:Field[];
  configuration:object;

  getFieldGrid() {
    let res:string[][] = [];
    if(this.fields.length<=4)return [this.fields.map((v)=>v.name)];
    const grid_height = (this.fields.length==6||this.fields.length==9 )?3:4;
    for(let i = 0; i<this.fields.length; i = i + grid_height){
      res.push([]);
      for(let j = 0; j<grid_height && j+i < this.fields.length; j++){
        if(this.fields[j+i].key=='overview')res[res.length-1].unshift(this.fields[j+i].name);
        else res[res.length-1].push(this.fields[j+i].name);
      }
    }
    if(/SYNC0/i.test(this.record['id']))return res; //Is a new record, no special features yet!
    let config = this.configuration;
    if(!config)return res; //No special configurations implemented yet!
    else{
      if(config['files'])res.push(['Files']);
      if(config['history'])res.push(['Logbook']);
    }
    return res;
  }

  getField(field: string) {
    for (const f of this.fields) {
      if(f.name==field)return f;
    }
    return this.fields[0]; //If none of the matches, we'll retrieve the first one.
  }
  digest_value(field:Field):string{
    let v = this.record[field.key];
    if(!field.optional){
      if(!v)throw `Field: ${field.name} is required.`;
      if(v.toString().trim()=='')throw `Field: ${field.name} is required`;
    }
    if(field.optional && !v)return null; //Is fine, an optional value is empty, nothing new or wrong here!
    v = v.toString().trim();
    if(PLACEHOLDER_FORMAT.test(v))throw `Field: ${field.name} is required. Make sure to update the template as appropriate.`
    if(field.field_type=='suggestion'||field.field_type=='select'){
      let opt = field.getSuggestions(this.record);
      if(!opt.includes(v))throw `Invalid value : ${v} for field: ${field.name}. Is not registered on the system.`;
    }
    if('map' in field.raw){
      //Mapping is required from text option to numeric ID. perform so:
      let RMap = field.raw.map;
      for (const m of RMap) {
        if(m.name.trim() == v)return m.id.toString;
      }
      throw `Could not map option:${v} to corresponding ID. Make sure you've selected a valid registered option.`;
    }
    switch (field.type) {
      case 'string':return `'${v}'`;
      case 'number': {
        let n = Number(v);
        if(isNaN(n))throw `Field: ${field.name} expects a numeric value. Got: ${v}.`;
        return v; //No quotes needed.
      }
      case 'date':{
        if(DATE_FORMAT.test(v)){
          return `to_date('${v}','mm/dd/yyyy')`;
        }else if(DATE_MONTH_FIRST_FORMAT.test(v)){
          v = v.replace(/[a-zA-Z]+/,(m)=>{
            m = m.toLowerCase();
            if(!(m in MONTH_LITERALS))throw `Invalid month: ${m}. Refer to value documentation for further details.`;
            return MONTH_LITERALS[m].toString();
          });
          return`to_date('${v}','mm/dd/yyyy')`;
        }else if(DATE_MONTH_SECOND_FORMAT.test(v)){
          v = v.replace(/[a-zA-Z]+/,(m)=>{
            m = m.toLowerCase();
            if(!(m in MONTH_LITERALS))throw `Invalid month: ${m}. Refer to value documentation for further details.`;
            return MONTH_LITERALS[m].toString();
          });
          return `to_date('${v}','dd/mm/yyyy')`;
        }else
        throw `Invalid date format for field: ${field.name}. Refer to value documentation for details on valid formats. Got: ${v}`;
      }
      default: throw `Unrecognized type: ${field.type} for field: ${field.name}. Value: ${v}`;
    }
  }
  compile() : string{
    //let query = {module:this.module};
    let query = 'update ';
      for (const field of this.fields) if(field.editable)
      query = `${query} ${field.key} = "${this.digest_value(field).replace('"','\\"')}",`;
      query = query.substring(0,query.length-1); //We delete the last comma.
      query +=  ' for '+Number(this.getId());
      return query;
  }

  getId() {
    return /[0-9]+/.exec(this.record['id'])[0];
  }
}
