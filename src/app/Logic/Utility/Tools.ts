
export const PAGE_SIZE = 25;

export function capitalize(str:string){
  let r = str.split('_');
  let aux = '';
  for (const string of r) {
    const ren = string.toLowerCase();
    aux += ren.charAt(0).toUpperCase()+ren.substring(1);
    aux+=' ';
  }
  return aux.trim();
}
export function toRoman(number: number) {
  if (isNaN(number))
    return '.';
  if(number<=0)return '0';
  let num = Math.floor(number);
  let digits = String(+num).split(""),
    key = ["","C","CC","CCC","CD","D","DC","DCC","DCCC","CM",
      "","X","XX","XXX","XL","L","LX","LXX","LXXX","XC",
      "","I","II","III","IV","V","VI","VII","VIII","IX"],
    roman = "",
    i = 3;
  while (i--)
    roman = (key[+digits.pop() + (i * 10)] || "") + roman;
  return Array(+digits.join("") + 1).join("M") + roman;
}
