import {Component, Input, OnChanges} from '@angular/core';
import {SebastianService} from '../../Services/sebastian.service';
import {Field} from '../../Utility/RDisplay';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {ContextService} from '../../Services/context.service';
@Component({
  selector: 'ct-field',
  templateUrl: './field-layout.component.html',
  styleUrls: ['./field-layout.component.css']
})
export class FieldLayoutComponent implements OnChanges {
  @Input() field : Field;
  type = 0;
  isEditing = false;
  checkingOverview = false;
  value : string;
  suggestions : string[];
  mySuggestionsControl = new FormControl();
  filteredOptions: Observable<string[]>;
  constructor(public Sebastian:SebastianService,public Context:ContextService) {
    this.filteredOptions = this.mySuggestionsControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this.filter_option(value))
      );
  }
  ngOnChanges() {
    switch (this.field.field_type) {
      case 'input' : this.type = 0; break;
      case 'select' : this.type = 1; break;
      case 'suggestion' : this.type = 2; break;
      case 'password' : this.type = -1; break;
      default: this.type = 0;
    }
    this.checkingOverview = this.field.key==='overview';
    this.isEditing = false;
    this.suggestions = this.field.getSuggestions(this.Context.display.record);
    this.value = this.Context.display.record[this.field.key];
    this.mySuggestionsControl.setValue(this.value);
  }
  toggle_edition(){
    if(this.isEditing){ //User pressed save.
      if(this.type==2)this.value = this.mySuggestionsControl.value;
      this.Context.display.record[this.field.key] = this.value; //Save value to record.
    }
    this.isEditing = !this.isEditing;
  }
  private filter_option(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.suggestions.filter(option => option.trim().toLowerCase().startsWith(filterValue));
  }
}
