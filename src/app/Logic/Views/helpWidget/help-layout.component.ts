import {Component, Input, OnChanges} from '@angular/core';
import {Help} from '../../Utility/Help';
import {capitalize} from "../../Utility/Tools";
import {SebastianService} from '../../Services/sebastian.service';
@Component({
  selector: 'ct-help',
  templateUrl: './help-layout.component.html',
  styleUrls: ['./help-layout.component.css']
})
export class HelpLayoutComponent implements OnChanges {
  @Input() help: Help;
  @Input() level : number;
  paragraphs : string[];
  constructor(public Sebastian:SebastianService) {}
  ngOnChanges() {
    this.paragraphs = this.help.description.body.split(/<br>/gi);
  }

  getHeaderClass() {
    function find_out(lvl:number){
      switch (lvl) {
        case 0:
        case 1:
          return 'HeaderLVL1';
        case 2: return 'HeaderLVL2';
        case 3: return 'HeaderLVL3';
        case 4: return 'HeaderLVL4';
        case 5: return 'HeaderLVL5';
        case 6: return 'HeaderLVL6';
        default: return 'HeaderLVL7';
      }
    }
    return `${find_out(this.level)} HelpHeader`;
  }

  getExampleClass() {
    function find_out(lvl:number){
      switch (lvl) {
        case 0:
        case 1:
          return 'SubHeaderLVL1';
        case 2: return 'SubHeaderLVL2';
        case 3: return 'SubHeaderLVL3';
        case 4: return 'SubHeaderLVL4';
        case 5: return 'SubHeaderLVL5';
        case 6: return 'SubHeaderLVL6';
        default: return 'SubHeaderLVL7';
      }
    }
    return `${find_out(this.level)} HelpSubHeader`;
  }
  getTitle() {
    return capitalize(this.help.description.name);
  }

  trigger_command(link: string) {
    this.Sebastian.command(link);
  }
}
