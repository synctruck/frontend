import {Component, Input, OnChanges} from '@angular/core';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import {capitalize} from '../../Utility/Tools';

@Component({
  selector: 'linked-lists',
  templateUrl: './linked-lists.component.html',
  styleUrls: ['./linked-lists.component.css']
})
export class LinkedListsComponent implements OnChanges {
  @Input() list1: string[];
  @Input() list2: string[];
  @Input() title1: string;
  @Input() title2: string;

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
  }

  constructor() {}
  ngOnChanges() {}

  cap(item: string) {
    return capitalize(item);
  }
}
