import {
  Directive, ElementRef,
  Input, Renderer2, OnInit, OnChanges
} from '@angular/core';
import {ColumnMap} from '../Utility/ColumnSetting';
import {ContextService} from '../Services/context.service';

@Directive({ selector: '[ctHeader]'})
export class StyleCellDirective implements OnChanges {
  @Input() ctHeader: ColumnMap;
  @Input() ctRecord: any;
  constructor(
    private el: ElementRef,
    private renderer: Renderer2,
    private Context:ContextService) { }
  save_ref(ref:any){
    let index = this.ctRecord['indice'];
    if(!this.Context.manual_records[index])this.Context.manual_records[index] = {};
    this.Context.manual_records[index][this.ctHeader.primaryKey] = ref;
  }
  ngOnChanges() {
    //Empty the element first!
    const childElements = this.el.nativeElement.childNodes;
    for (const child of childElements) {
      this.renderer.removeChild(this.el.nativeElement, child);
    }
    const background = this.ctHeader.get_background(this.ctRecord);
    if(background)this.renderer.setStyle(this.el.nativeElement,'background',background);
    this.renderer.setStyle(this.el.nativeElement,'padding','5px 10px');
    const cell_type = this.ctHeader.get_type();
    if(cell_type==='input'){
      const inp = this.renderer.createElement('input');
      this.renderer.setStyle(inp,'width','calc(100% - 15px)');
      this.renderer.setProperty(inp,'value',this.ctRecord[this.ctHeader.access(this.ctRecord)]);
      this.save_ref(inp);
      this.renderer.appendChild(this.el.nativeElement, inp);
      return;
    }
    if(cell_type==='select'){
      const opts = this.ctHeader.get_options();
      const select = this.renderer.createElement('select');
      for (const o of opts) {
        const opt = this.renderer.createElement('option');
        this.renderer.setProperty(opt,'value',o);
        const t = this.renderer.createText(o);
        this.renderer.appendChild(opt,t);
        this.renderer.appendChild(select,opt);
      }
      const v = this.ctRecord[this.ctHeader.access(this.ctRecord)];
      if(v){
        if(opts.includes(v))this.renderer.setProperty(select,'value',v);
      }
      this.ctRecord = this.save_ref(select);
      this.renderer.appendChild(this.el.nativeElement,select);
      return;
    }
    const label = this.renderer.createElement('label');
    let content = this.ctRecord[this.ctHeader.access(this.ctRecord)];
    if(this.ctHeader.primaryKey=='password')content = [...content].map(()=>'*').join(''); //We hide the password.
    if(content){
      this.renderer.appendChild(label,this.renderer.createText(content));
      if(this.ctHeader.get_link(this.ctRecord)){
        this.renderer.addClass(label,'RLink');
      }else{
        this.renderer.setStyle(label,'color',this.ctHeader.get_color(this.ctRecord));
      }
    }else{
      this.renderer.appendChild(label,this.renderer.createText('Empty'));
      this.renderer.setStyle(label,'color','gray');
    }
    this.renderer.appendChild(this.el.nativeElement,label);
  }
}
