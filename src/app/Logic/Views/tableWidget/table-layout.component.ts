import {Component, Input, OnChanges} from '@angular/core';
import {ColumnMap} from "../../Utility/ColumnSetting";
import {SebastianService} from '../../Services/sebastian.service';

@Component({
  selector: 'ct-table',
  templateUrl: './table-layout.component.html',
  styleUrls: ['./table-layout.component.css']
})
export class TableLayoutComponent implements OnChanges {
  @Input() records: any[];
  @Input() caption: string;
  @Input() settings: any[];
  columnMaps: ColumnMap[];
  constructor(public Sebastian:SebastianService) {}

  ngOnChanges() {
    if (this.settings) {
      this.columnMaps = this.settings
        .map( col => new ColumnMap(col) );
    } else {
      this.columnMaps = Object.keys(this.records[0])
        .map( key => {
          return new ColumnMap( { primaryKey: key });
        });
    }
  }

  clickedLink(record:any,header:ColumnMap) {
    let command = header.get_link(record);
    if(!command)return;
    if(command.startsWith('http')) return window.open(command, "_blank");
    this.Sebastian.command(command);
  }
}
