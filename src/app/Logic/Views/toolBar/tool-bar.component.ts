import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog,MatDialogRef,MAT_DIALOG_DATA} from '@angular/material/dialog';
import {RModalComponent} from '../../../rmodal/rmodal.component';

import {SebastianService} from '../../Services/sebastian.service';
import {ContextService} from '../../Services/context.service';
import {map, startWith} from 'rxjs/operators';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {capitalize} from '../../Utility/Tools';
import {hasInjectableDecorator} from '@angular/core/schematics/migrations/undecorated-classes-with-di/ng_declaration_collector';

const MAX_COMMANDS = 100;
@Component({
  selector: 'tool-bar',
  templateUrl: './tool-bar.component.html',
  styleUrls: ['./tool-bar.component.css']
})
export class ToolBarComponent implements OnInit {
  @ViewChild('SourceInput')
  SourceInput: any;
  public command_history : string[];
  mySourceControl = new FormControl();
  filteredOptions: Observable<string[]>;
  logo = 'https://assets-synctruck.s3.us-east-2.amazonaws.com/logo-small.jpg';

  constructor(public Sebastian:SebastianService, public Context : ContextService,public dialog: MatDialog) {
    this.enableCommand = false;
    this.command_history = [];
    this.filteredOptions = this.mySourceControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this.filter_history(value))
      );
  }
  enableCommand : boolean;
  ngOnInit() {}

  public compile_source_command(){
    this.Sebastian.command(this.mySourceControl.value);
    this.save_command(this.mySourceControl.value.trim());
  }
  public save_command(cmd:string){
    if(this.command_history.length>0)if(cmd.toLowerCase() === this.command_history[0].toLowerCase())return; //If the command is the same, don't bother.
    this.command_history.unshift(cmd);
    if(this.command_history.length>MAX_COMMANDS)this.command_history.pop();
  }
  private filter_history(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.command_history.filter(option => option.trim().toLowerCase().startsWith(filterValue));
  }

  logout() {
    this.Sebastian.logout();
  }


  cap(p: string) {
   return capitalize(p);
  }

  takeAction(p: string) {
    if(this.Sebastian.takeAction(p)){
      const dialogRef = this.dialog.open(RModalComponent, {
        data: { command : p}
      });
      dialogRef.afterClosed().subscribe(result => result?result.trim()!=''?this.Sebastian.command(result):undefined:p=='edit'?this.Sebastian.command('edit'):undefined);
    }
  }
}
