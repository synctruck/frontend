import {ContextService} from '../../Services/context.service';
import {Component, Input, OnChanges} from '@angular/core';
import {SebastianService} from '../../Services/sebastian.service';
import {Option} from '../../Grammar/Option';
import {capitalize, toRoman} from '../../Utility/Tools';
import { trigger, state, style, animate, transition } from '@angular/animations';
import {MatDialog,MatDialogRef,MAT_DIALOG_DATA} from '@angular/material/dialog';
import {RModalComponent} from '../../../rmodal/rmodal.component';

@Component({
  selector: 'vertical-nav',
  templateUrl: './vertical-nav.component.html',
  styleUrls: ['./vertical-nav.component.css'],
  animations: [
    trigger(
      'inOutAnimation',
      [
        transition(
          ':enter',
          [
            style({ height: 0, width : 205 }),
            animate('2s ease-out',
              style({ height: 500, width : 205 }))
          ]
        )
      ]
    )
  ]
})
export class VerticalNavComponent implements OnChanges {
  constructor(public Sebastian:SebastianService,public Context : ContextService,public dialog: MatDialog) {}
  public selectedModule:Option = new Option('loading','loading');
  @Input() action: string;
  buttons: Array<string>;

  ngOnChanges() {
    this.buttons = ['manual_mode','commit','cleanse','purge','delete','save'];
    if (this.action=='workspace'){
      const aux = this.buttons.slice(0,2);
      this.buttons = this.buttons.slice(2);
      this.buttons.unshift(...aux,'create_1','create_many','fuse','clone');
    }
  }
  public onSelectModule(module:Option){
    this.selectedModule = module;
    this.Sebastian.command(`[goto] ${module.id} 1`);
  }
  public onStationChange(){
    this.Sebastian.command(`[update-station] ${this.Context.selected_station.id}`);
  }

  Tools_toRoman(number: number) {
    return toRoman(number);
  }
  Tools_capitalize(name: string) {
    return capitalize(name);
  }

  public openModal(hint : string){
    const dialogRef = this.dialog.open(RModalComponent, {
      data: {command : hint}
    });
    dialogRef.afterClosed().subscribe(result => result?result.trim()!=''?this.Sebastian.command(result):undefined:hint=='edit'?this.Sebastian.command('edit'):undefined);
  }

  performAction(t: string) {
    if(this.Sebastian.performAction(t))this.openModal(t);
  }

  takeAction(other: string) {
    if(this.Sebastian.takeAction(other))this.openModal(other);
  }
}
