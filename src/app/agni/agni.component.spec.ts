import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgniComponent } from './agni.component';

describe('AgniComponent', () => {
  let component: AgniComponent;
  let fixture: ComponentFixture<AgniComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgniComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
