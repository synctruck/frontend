import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {RModalComponent} from '../rmodal/rmodal.component';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-agni',
  templateUrl: './agni.component.html',
  styleUrls: ['./agni.component.css']
})
export class AgniComponent implements OnInit {

  constructor(public dialog: MatDialog) { }

  ngOnInit(): void { }

  public openModal(hint : string): Observable<any>{
    const dialogRef = this.dialog.open(RModalComponent, {
      data: {command : hint}
    });
    return dialogRef.afterClosed();
  }
}
