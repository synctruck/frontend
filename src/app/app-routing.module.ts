import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SearchComponent} from './search/search.component';
import {HelpComponent} from './help/help.component';
import {DisplayComponent} from './display/display.component';
import {LogbookComponent} from './logbook/logbook.component';
import {ManualComponent} from './manual/manual.component';
import {ImportarComponent} from './importar/importar.component';
import {ModuleFilesComponent} from './module-files/module-files.component';

const routes: Routes = [
  {
    path:'',
    component: SearchComponent
  },
  {
    path : 'files',
    component : ModuleFilesComponent
  },
  {
    path:'search',
    component: SearchComponent
  },
  {
    path:'help',
    component:HelpComponent
  },
  {
    path:'display',
    component:DisplayComponent
  },
  {
    path:'logbook',
    component:LogbookComponent
  },
  {
    path:'manual',
    component:ManualComponent
  },
  {
    path :'importar',
    component : ImportarComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
