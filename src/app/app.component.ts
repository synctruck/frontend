import {Component, HostListener, ViewChild} from '@angular/core';
import {SebastianService} from './Logic/Services/sebastian.service';
import { FormBuilder,FormControl} from '@angular/forms';
import {ContextService} from './Logic/Services/context.service';
import {Title} from '@angular/platform-browser';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Synctruck Portal';
  public RouterContainerWidth:number;
  constructor(private titleHelper:Title, public Sebastian:SebastianService,public Context:ContextService,private formBuilder: FormBuilder) {
    this.getScreenSize();
    this.loginForm = this.formBuilder.group({username:'',password:''});
    this.wallpaper = 'https://assets-synctruck.s3.us-east-2.amazonaws.com/Login_wallpaper.jpg';
    this.titleHelper.setTitle(this.title);
  }
  //region login
  screenHeight: number;
  screenWidth: number;
  loginForm;
  wallpaper;
  login() {
    try{
      const a = this.loginForm.value;
      if(!a.username || !a.password)return;
      this.Sebastian.login(a).then((data)=>{
        this.Context.token = data.token;
        this.Context.primary = data.primary;
        this.Context.secondary = data.secondary;
        this.Context.available_tables = data.tables ? data.tables : [];
        this.Sebastian.load_defaults();
      }).catch((err)=>{
        this.Sebastian.error(err.message);
      });
    }catch(err){
      this.Sebastian.error(err);
    }
  }
  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) { //618 1366 //Ideal: 80% for router container. 205px exactos for Vertical Nav.
    this.screenHeight = Math.ceil(window.innerHeight*0.97);
    this.screenWidth = Math.ceil(window.innerWidth*0.99);
    const right_margin = this.screenWidth*4/100;
    this.RouterContainerWidth  = this.screenWidth - 205 - right_margin ;
  }
  //endregion

}
