import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HelpComponent } from './help/help.component';
import { SearchComponent } from './search/search.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import { HttpClientModule } from '@angular/common/http';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatMenuModule} from '@angular/material/menu';
import {ToastrModule} from 'ngx-toastr';
import {MatTableModule} from '@angular/material/table';
import {MatIconModule} from '@angular/material/icon';
import {MatDividerModule} from '@angular/material/divider';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatSelectModule} from '@angular/material/select';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { StyleCellDirective } from './Logic/Views/style-cell.directive';
import {SafeHTMLPipe} from './Logic/Views/safe-html.pipe'
import {TableLayoutComponent} from './Logic/Views/tableWidget/table-layout.component';
import { CurrencyPipe } from '@angular/common';
import { FormatCellPipe } from './Logic/Views/format-cell.pipe';
import {HelpLayoutComponent} from './Logic/Views/helpWidget/help-layout.component';
import { DisplayComponent } from './display/display.component';
import {FieldLayoutComponent} from './Logic/Views/fieldWidget/field-layout.component';
import { LogbookComponent } from './logbook/logbook.component';
import { ManualComponent } from './manual/manual.component';
import { ImportarComponent } from './importar/importar.component';
import { LinkedListsComponent} from './Logic/Views/linkedLists/linked-lists.component';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {ToolBarComponent} from './Logic/Views/toolBar/tool-bar.component';
import {VerticalNavComponent} from './Logic/Views/verticalNav/vertical-nav.component';
import { RModalComponent } from './rmodal/rmodal.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MaterialFileUploadComponent} from './material-file-upload/material-file-upload.component';
import { ModuleFilesComponent } from './module-files/module-files.component';
import { AgniComponent } from './agni/agni.component';

@NgModule({
  declarations: [
    AppComponent,
    HelpComponent,
    SearchComponent,
    StyleCellDirective,
    SafeHTMLPipe,
    TableLayoutComponent,
    FormatCellPipe,
    HelpLayoutComponent,
    DisplayComponent,
    FieldLayoutComponent,
    LogbookComponent,
    ManualComponent,
    ImportarComponent,
    LinkedListsComponent,
    ToolBarComponent,
    VerticalNavComponent,
    RModalComponent,
    MaterialFileUploadComponent,
    ModuleFilesComponent,
    AgniComponent
  ],
  imports: [
    AppRoutingModule,
    MatInputModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatToolbarModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    MatTableModule,
    MatDividerModule,
    MatProgressSpinnerModule,
    FormsModule,
    ReactiveFormsModule,
    ToastrModule.forRoot(),
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    MatProgressBarModule,
    MatAutocompleteModule,
    DragDropModule,
    MatDialogModule,
    MatCheckboxModule
  ],
  providers: [CurrencyPipe],
  bootstrap: [AppComponent],
  entryComponents : [RModalComponent]
})
export class AppModule { }
