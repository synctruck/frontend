import { Component, OnInit } from '@angular/core';
import {SebastianService} from '../Logic/Services/sebastian.service';
import {Field, RDisplay} from '../Logic/Utility/RDisplay';
import {ContextService} from '../Logic/Services/context.service';

@Component({
  selector: 'app-display',
  templateUrl: './display.component.html',
  styleUrls: ['./display.component.css']
})
export class DisplayComponent implements OnInit {
  selectedField:Field;

  constructor(public Sebastian:SebastianService,public Context:ContextService) {
    if(Context.display)this.selectedField = Context.display.getField('Overview');
  }
  ngOnInit(): void {}
  selectField(field:string){
    if(field.toLowerCase()==='files') {
      this.Sebastian.getFiles(Number(this.Context.display.getId())).then(v => {
        this.Sebastian.display_files(v);
      }).catch(err => this.Sebastian.error(err.message));
    }
    if(field.toLowerCase()==='logbook'){
      this.Sebastian.command('[logbook] '+this.Context.display.getId());
      return;
    }
    this.selectedField = this.Context.display.getField(field);
  }
}
