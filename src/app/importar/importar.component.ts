import { Component, OnInit } from '@angular/core';
import {SebastianService} from '../Logic/Services/sebastian.service';
import {ContextService} from '../Logic/Services/context.service';

@Component({
  selector: 'app-importar',
  templateUrl: './importar.component.html',
  styleUrls: ['./importar.component.css']
})
export class ImportarComponent implements OnInit {
  new_sheet_url: string;
  index : number;
  constructor(public Sebastian:SebastianService,public Context:ContextService) {
    this.new_sheet_url = '';
    this.index = 0;
  }

  ngOnInit(): void {}
  getSheets(spreadsheetIndex : number){
    this.Sebastian.selectSheet(spreadsheetIndex);
  }

  register_sheet(){
    const parts = new RegExp("/spreadsheets/d/([a-zA-Z0-9-_]+)").exec(this.new_sheet_url);
    let id : string;
    if(!parts) return this.Sebastian.error('Invalid Spreadsheet URL');
    else id = parts[1];
    if(!id)return this.Sebastian.error('Invalid Spreadsheet URL');
    this.Sebastian.registerNewSheet(id);
  }

  processSheet(sheet: string,action : string) {
    this.Sebastian.processSheet(this.index,sheet,action);
  }
}
