import { Component, OnInit } from '@angular/core';
import {SebastianService} from '../Logic/Services/sebastian.service';
import {Log} from '../Logic/Utility/Log';
import {ContextService} from '../Logic/Services/context.service';

@Component({
  selector: 'app-logbook',
  templateUrl: './logbook.component.html',
  styleUrls: ['./logbook.component.css']
})
export class LogbookComponent implements OnInit {
  constructor(public Context:ContextService) {}
  ngOnInit(): void {}
}
