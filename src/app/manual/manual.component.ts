import { Component, OnInit } from '@angular/core';
import {ContextService} from '../Logic/Services/context.service';

@Component({
  selector: 'app-manual',
  templateUrl: './manual.component.html',
  styleUrls: ['./manual.component.css']
})
export class ManualComponent implements OnInit {

  constructor(public Context:ContextService) {}
  ngOnInit(): void {
    this.Context.manual_data = this.Context.table;
    this.Context.manual_records = {}; //We empty the manual records.
    this.Context.manual_headers = this.Context.tableSettings;
  }

}
