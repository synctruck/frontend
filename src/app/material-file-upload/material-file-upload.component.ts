import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { HttpClient, HttpResponse, HttpRequest,
         HttpEventType, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { of } from 'rxjs';
import { catchError, last, map, tap } from 'rxjs/operators';
import {SebastianService} from '../Logic/Services/sebastian.service';
import {ContextService} from '../Logic/Services/context.service';

@Component({
  selector: 'app-material-file-upload',
  templateUrl: './material-file-upload.component.html',
  styleUrls: ['./material-file-upload.component.css'],
      animations: [
            trigger('fadeInOut', [
                  state('in', style({ opacity: 100 })),
                  transition('* => void', [
                        animate(300, style({ opacity: 0 }))
                  ])
            ])
      ]
})
export class MaterialFileUploadComponent implements OnInit {
  file_name : string = '';
  allowed_types = {
    text: /text\/.+/,
    image: /image\/.+/,
    document : /application\/pdf/,
    audio : /audio\/.+/,
    video : /video\/.+/,
    compressed : /application\/((gzip)|(zip)|(jar)|(rar)|(egg))/
  };
  verify_required_fields():boolean {
    if(this.files.length != 0){
      this.Sebastian.error('You can only upload one file at a time.');
      return false;
    }
    return true;
  }
      /** Link text */
      @Input() text = 'Upload';
      @Input() profile : string = null;
      @Input() accept = '*/*';
      /** Allow you to add handler after its completion. Bubble up response text from remote. */
      @Output() complete = new EventEmitter<string>();

      public files: Array<FileUploadModel> = [];

      constructor(private _http: HttpClient, public Sebastian:SebastianService, public Context:ContextService) { }

      ngOnInit() {
      }

      onClick() {
        if(!this.verify_required_fields()) return; //File name & description must be supplied
            const fileUpload = document.getElementById('fileUpload') as HTMLInputElement;
            fileUpload.onchange = () => {
                  for (let index = 0; index < fileUpload.files.length; index++) {
                        const file = fileUpload.files[index];
                        this.files.push({ data: file, state: 'in',
                          inProgress: false, progress: 0, canRetry: false, canCancel: false });
                  }
                  this.uploadFiles();
            };
            //I should probably verify that the file is indeed the
            //One supplied by the file name.
            fileUpload.click();
      }

      cancelFile(file: FileUploadModel) {
            file.sub.unsubscribe();
            this.removeFileFromArray(file);
      }

      retryFile(file: FileUploadModel) {
            this.uploadFile(file);
            file.canRetry = false;
      }

      private async uploadFile(file: FileUploadModel) {
        let fname = file.data.name;
        let size = file.data.size; //In bytes
        let ftype = file.data.type; //True File Type
        let jm : string; //File type
        const forbidden_chars = /[^0-9a-zA-Z!_.()\s-]+/g;
        this.file_name = fname.replace(forbidden_chars,'-').replace(/\s+/g,'_');
        for(const [a,b] of Object.entries(this.allowed_types)){
          if (b.test(ftype)){ jm = a; break;}
        }
        if(!jm){
          this.Sebastian.error(`For security reasons, ${ftype} files are forbidden.`);
          this.removeFileFromArray(file);
          return;
        }
        const fbody = {
          module : this.Context.module,
          session : Number(this.Context.token),
          id : Number(this.Context.display.getId()),
          file : this.file_name,
          type : jm,
          size : size,
          profile: this.profile != null
        };
        //Perform http request to get presigned url.
        let url : string = await this.Sebastian.registerFile(fbody);
        if (!url){
          this.Sebastian.error('An error ocurred while uploaing the file. Please, try again later.');
          this.removeFileFromArray(file);
          return;
        }
            const req = new HttpRequest('PUT', url, file.data, {
                  reportProgress: true
            });

            file.inProgress = true;
            file.sub = this._http.request(req).pipe(
                  map(event => {
                        switch (event.type) {
                              case HttpEventType.UploadProgress:
                                    file.progress = Math.round(event.loaded * 100 / event.total);
                                    break;
                              case HttpEventType.Response:
                                    return event;
                        }
                  }),
                  tap(message => { }),
                  last(),
                  catchError((error: HttpErrorResponse) => {
                        file.inProgress = false;
                        file.canRetry = true;
                        return of(`${file.data.name} upload failed.`);
                  })
            ).subscribe(
                  (event: any) => {
                        if (typeof (event) === 'object') {
                              //File upload finished!
                              //Clear file name & description to start over!
                              this.file_name = '';
                              this.removeFileFromArray(file);
                              this.complete.emit(event.body);
                        }
                  }
            );
      }

      private uploadFiles() {
            const fileUpload = document.getElementById('fileUpload') as HTMLInputElement;
            fileUpload.value = '';

            this.files.forEach(file => {
                  this.uploadFile(file);
            });
      }

      private removeFileFromArray(file: FileUploadModel) {
            const index = this.files.indexOf(file);
            if (index > -1) {
                  this.files.splice(index, 1);
            }
      }

}

export class FileUploadModel {
      data: File;
      state: string;
      inProgress: boolean;
      progress: number;
      canRetry: boolean;
      canCancel: boolean;
      sub?: Subscription;
}
