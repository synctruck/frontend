import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModuleFilesComponent } from './module-files.component';

describe('ModuleFilesComponent', () => {
  let component: ModuleFilesComponent;
  let fixture: ComponentFixture<ModuleFilesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModuleFilesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModuleFilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
