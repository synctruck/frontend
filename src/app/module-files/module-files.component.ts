import { Component, OnInit } from '@angular/core';
import {SebastianService} from '../Logic/Services/sebastian.service';
import {ContextService} from '../Logic/Services/context.service';

@Component({
  selector: 'app-module-files',
  templateUrl: './module-files.component.html',
  styleUrls: ['./module-files.component.css']
})
export class ModuleFilesComponent implements OnInit {
  fileSettings = [
    {primaryKey: 'name', header: 'File Name', link: 'url' },
    {primaryKey: 'type',header:'Type'},
    {primaryKey: 'size',header:'Size'},
    {primaryKey: 'date_uploaded',header:'Uploaded Date'},
    {primaryKey: 'uploaded_by',header:'Uploaded By'}
  ];
  constructor(public Sebastian:SebastianService, public Context:ContextService) { }

  ngOnInit(): void {}


  refresh($event: string) {
   console.log($event);
   this.Sebastian.getFiles(Number(this.Context.display.getId())).then(v=>{
     this.Context.filesBuffer = v;
   });
  }
}
