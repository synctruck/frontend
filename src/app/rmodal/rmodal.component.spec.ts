import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RModalComponent } from './rmodal.component';

describe('RModalComponent', () => {
  let component: RModalComponent;
  let fixture: ComponentFixture<RModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
