import { Component, OnInit,Inject } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {capitalize} from '../Logic/Utility/Tools';
import {ContextService} from '../Logic/Services/context.service';

interface DialogData {
  command: string;
  id ?: string;
  options ?: Array<any>;
}
@Component({
  selector: 'app-rmodal',
  templateUrl: './rmodal.component.html',
  styleUrls: ['./rmodal.component.css']
})
export class RModalComponent implements OnInit {
  constructor( public dialogRef: MatDialogRef<RModalComponent>,
  @Inject(MAT_DIALOG_DATA) public data: DialogData,public Context : ContextService) {}
  title : string;
  hint : string;
  num_range : Array<number>;
  num : number;
  target_num : any;
  indexes : Array<any>;
  reset: boolean;
  misc : string;
  direction : string;
  sOption: string;
  checkBoxes : Array<any>;
  val: string;
  op : string;
  allSelected : boolean;
  aliases = {
  'Less than': '<',
  'Less or equal than': '<=',
  'Greater than': '>',
  'Greater or equal than':'>=',
  'Equal':'=',
  'Not equal':'!='
  };
  public readonly ops : Array<string> = [
    'starts with', 'ends with', 'contains',
    'from','from - to',
    'Less than','Higher than','Less or equal than','Greater or queal than','Equal','Not equal',
    'in','is empty','is not empty'
  ];
  escape_value(v){
    if(v.toString()[0]=="'"||v.toString()[0]=='"')return v; //Is already escaped!!
    return  ['starts with','ends with','contains'].includes(this.op)?`'${v}'`:
    isNaN(Number(v))? /\/[0-9]{2}/.test(v)?v:`'${v}'` : v;
  }
  val2: string;
  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {
    this.sOption = '1';
    this.allSelected = true;
    const d = new Date();
    this.val2 = `${d.getMonth()}/${d.getDate()}/${d.getFullYear()}`;
    this.op = this.ops[0];
    this.indexes = this.Context.table.map(t=>((k)=>k?/sync/i.test(k.toString())?k:'Sync'+k:undefined)(t.id) || t.indice + 'th');
    this.checkBoxes = this.indexes.map(i=>{ return {id : i, selected : true}; });
    this.target_num = this.indexes[0];
    this.num = 5;
    this.num_range = [5,10,15,20,25,40,50,75,100];
    this.reset = false;
    this.hint = this.data.command;
    if(this.hint=='order'||this.hint=='filter')this.misc = this.Context.tableSettings[0]['header'];
    this.title = capitalize(this.data.command);
    this.direction = 'ASC';
  }
  public selectOption(opt : string){
    this.sOption = opt;
    return this.compileCommand();
  }
  compileCommand() {
    switch (this.hint){
      case 'edit':
      case 'delete' : {
        const aux = [];
        for (const check of this.checkBoxes) {
          if(check.selected)aux.push(check.id);
        }
        return `${this.hint} ${aux.join(',')} rows`;
      }
      case 'filter' :{
        const prefix = this.reset?'reset filter ':'filter ';
        if(this.reset){
          if(this.val){
            if (this.val.trim() == '') return 'reset';
          }else return 'reset';
        }else{
          if(!this.val)return ''; //If reset is not marked but val is not defined we do nothing.
          if(this.val.trim()=='')return '';
        }
        let true_op = this.op;
        if (true_op in this.aliases) true_op = this.aliases[true_op]; //We translate aliases if any.
        if(/age[^a-zA-Z]/i.test(this.misc))this.misc = 'age';
        if(true_op=='is empty' || true_op == 'is not empty')return `${prefix} ${this.misc} ${true_op}`;
        if(true_op=='from - to')return `${prefix} ${this.misc} from ${this.val} to ${this.val2}`;
        if(true_op=='in'){
          if(!this.val)return 'reset';

          const values = this.val.split(',').map(v=>this.escape_value(v));
          return `${prefix} ${this.misc} in (${values.join(',')})`;
        }
        else{
          const v = this.val;
          return `${prefix} ${this.misc} ${true_op} ${this.escape_value(v)}`;
        }
      }
      case 'clone' : return `clone ${this.target_num} row ${this.num} times`;
      case 'create_many': return `prepare ${this.num} rows`;
      case 'order' : return `${this.reset?'reset':''} order by ${/age/i.test(this.misc)?'age':this.misc} ${this.direction}`;
      case 'change_applicant_status':{
       return `update status = ${this.sOption} for ${this.data.id}`;
      }
      default : return '??';
    }
  }

  toggleSelection() {
    for(let i = 0; i<this.checkBoxes.length; i++){
      this.checkBoxes[i].selected = this.allSelected;
    }
  }
}
