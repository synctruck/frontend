import { Component, OnInit } from '@angular/core';
import {SebastianService} from '../Logic/Services/sebastian.service';
import {ContextService} from '../Logic/Services/context.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  constructor(public Sebastian:SebastianService,public Context:ContextService) { }

  ngOnInit(): void {}
  next(){
    this.Sebastian.nextPage();
  }
  prev() {
    this.Sebastian.prevPage();
  }

  getUpperIndex() {
    return (this.Context.index+this.Context.pageSize > this.Context.recordCount)?this.Context.recordCount:this.Context.index + this.Context.pageSize;
  }
}
